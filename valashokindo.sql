-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 26, 2020 at 10:53 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `valashokindo`
--

-- --------------------------------------------------------

--
-- Table structure for table `partner`
--

CREATE TABLE `partner` (
  `id_partner` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(150) NOT NULL,
  `username` varchar(50) NOT NULL,
  `partner_name` varchar(100) DEFAULT NULL,
  `partner_uuid` varchar(50) NOT NULL,
  `partner_phone` varchar(500) NOT NULL,
  `transaction_count` int(200) NOT NULL,
  `store_id` int(15) NOT NULL,
  `token` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `partner`
--

INSERT INTO `partner` (`id_partner`, `email`, `password`, `username`, `partner_name`, `partner_uuid`, `partner_phone`, `transaction_count`, `store_id`, `token`) VALUES
(5, 'admin@gmail.com', 'adminvalala', 'valala', 'valashokindo', 'iajwiejf-aiwejf-askdjfkw1293-1939', '085153543', 25, 1, 'e17134e0cff6f7c2e67ea79e2f63acff');

-- --------------------------------------------------------

--
-- Table structure for table `rate`
--

CREATE TABLE `rate` (
  `id_rate` int(11) NOT NULL,
  `currency` varchar(50) NOT NULL,
  `sell_price` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rate`
--

INSERT INTO `rate` (`id_rate`, `currency`, `sell_price`) VALUES
(1, 'USD', '16500'),
(2, 'HKD', '1780');

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE `stock` (
  `id_stock` int(11) NOT NULL,
  `stock_currency` varchar(100) NOT NULL,
  `stock_total` varchar(100) NOT NULL,
  `stock_store` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock`
--

INSERT INTO `stock` (`id_stock`, `stock_currency`, `stock_total`, `stock_store`) VALUES
(1, 'HKD', '100', '1'),
(2, 'USD', '600', '1');

-- --------------------------------------------------------

--
-- Table structure for table `store`
--

CREATE TABLE `store` (
  `id_store` int(11) NOT NULL,
  `store_name` varchar(100) NOT NULL,
  `store_address` varchar(200) NOT NULL,
  `store_long` varchar(100) NOT NULL,
  `store_lat` varchar(100) NOT NULL,
  `store_image` varchar(500) NOT NULL,
  `store_open` varchar(100) NOT NULL,
  `store_close` varchar(100) NOT NULL,
  `store_stock` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `store`
--

INSERT INTO `store` (`id_store`, `store_name`, `store_address`, `store_long`, `store_lat`, `store_image`, `store_open`, `store_close`, `store_stock`) VALUES
(1, 'ValasHokindo', 'Ruko Sentra Prima PDD No.34, Sutorejo Prima Utara, Kalisari, Kec. Mulyorejo, Kota SBY, Jawa Timur 60112', '-7.260044', '112.794966', '', '08.00', '10.00', '700');

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE `transaction` (
  `id_transaction` int(11) NOT NULL,
  `transaction_ref` varchar(100) NOT NULL,
  `currency` varchar(10) NOT NULL,
  `exchange_amount` varchar(100) NOT NULL,
  `exchange_paid` varchar(100) NOT NULL,
  `rate` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `status` varchar(100) NOT NULL,
  `store_picked` int(15) NOT NULL,
  `user_name` varchar(150) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `flag` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaction`
--

INSERT INTO `transaction` (`id_transaction`, `transaction_ref`, `currency`, `exchange_amount`, `exchange_paid`, `rate`, `date`, `status`, `store_picked`, `user_name`, `user_email`, `flag`) VALUES
(1, 'VH3079', '', '500', '890000', '1780', '2020-03-25', 'on process', 0, '', 'coba2@gmail.com', 0),
(4, 'VH1261', '', '600', '1068000', '1780', '2020-03-25', 'on process', 0, '', 'coba2@gmail.com', 0),
(5, 'VH3170', '', '600', '1068000', '1780', '2020-03-25', 'on process', 0, '', 'coba2@gmail.com', 0),
(6, 'VH7430', '', '350', '623000', '1780', '2020-03-25', 'on process', 0, '', 'coba2@gmail.com', 0),
(7, 'VH7203', '', '355', '631900', '1780', '2020-03-25', 'on process', 0, '', 'coba2@gmail.com', 0),
(8, 'VH3770', '', '400', '712000', '1780', '2020-03-25', 'on process', 0, '', 'coba2@gmail.com', 0),
(9, 'VH9281', 'HKD', '400', '712000', '1780', '2020-03-25', 'success', 1, '', 'coba2@gmail.com', 1),
(10, 'VH8847', '', '400', '712000', '1780', '2020-03-25', 'success', 0, '', 'coba2@gmail.com', 0),
(11, 'VH3756', '', '400', '712000', '1780', '2020-03-25', 'success', 0, '', 'coba2@gmail.com', 1),
(12, 'VH4025', '', '400', '712000', '1780', '2020-03-25', 'success', 0, '', 'coba2@gmail.com', 1),
(13, 'VH3329', '', '400', '712000', '1780', '2020-03-25', 'on process', 0, '', 'coba2@gmail.com', 0),
(14, 'VH3193', '', '400', '712000', '1780', '2020-03-25', 'on process', 0, '', 'coba2@gmail.com', 0),
(15, 'VH6156', '', '400', '712000', '1780', '2020-03-25', 'on process', 0, '', 'coba2@gmail.com', 0),
(16, 'VH2824', '', '400', '712000', '1780', '2020-03-25', 'on process', 0, '', 'coba2@gmail.com', 0),
(17, 'VH8490', '', '400', '712000', '1780', '2020-03-25', 'on process', 0, '', 'coba2@gmail.com', 0),
(18, 'VH6613', '', '400', '712000', '1780', '2020-03-25', 'on process', 0, '', 'coba2@gmail.com', 0),
(19, 'VH1784', 'HKD', '100', '178000', '1780', '2020-03-26', 'success', 1, 'cobacobaa', 'coba2@gmail.com', 1),
(20, 'VH3241', 'HKD', '150', '267000', '1780', '2020-03-26', 'success', 1, 'cobacobaa', 'coba2@gmail.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(150) NOT NULL,
  `name` varchar(50) NOT NULL,
  `refferal` varchar(50) DEFAULT NULL,
  `user_uuid` varchar(50) NOT NULL,
  `transaction_count` int(200) NOT NULL,
  `token` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `email`, `password`, `name`, `refferal`, `user_uuid`, `transaction_count`, `token`) VALUES
(1, 'admin@testing.com', 'valalala', 'admin', '-', 'uaweijfaskdfj', 0, ''),
(2, 'coba@gmail.com', 'blablabla', 'cobacoba', '0', '04f5c48a-b490-453f-8ef2-0a595d7d482a', 0, ''),
(3, 'coba2@gmail.com', '$2y$10$6Rq.2CLaEiOYFltX8P37ZOtkpgma8djziB//JOv4XG9PoL9noQbom', 'cobacobaa', '0', 'f10dddea-9838-47cc-a276-af1463ec44a2', 6, 'b6b428dfb5dfe14add7bc187b9e01381'),
(4, 'coba3@gmail.com', '$2y$10$DmsFX4.CNW0mv1jyOMiTBew.8HL8HdGy0IakR8wfnStDQuuKRi5m.', 'budi lestiono', '0', '2c119377-d502-4aaa-b0e6-1811dce51f81', 0, '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `partner`
--
ALTER TABLE `partner`
  ADD PRIMARY KEY (`id_partner`);

--
-- Indexes for table `rate`
--
ALTER TABLE `rate`
  ADD PRIMARY KEY (`id_rate`);

--
-- Indexes for table `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`id_stock`);

--
-- Indexes for table `store`
--
ALTER TABLE `store`
  ADD PRIMARY KEY (`id_store`);

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`id_transaction`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `partner`
--
ALTER TABLE `partner`
  MODIFY `id_partner` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `rate`
--
ALTER TABLE `rate`
  MODIFY `id_rate` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `stock`
--
ALTER TABLE `stock`
  MODIFY `id_stock` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `store`
--
ALTER TABLE `store`
  MODIFY `id_store` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `transaction`
--
ALTER TABLE `transaction`
  MODIFY `id_transaction` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
