/* Dore Theme Select & Initializer Script 

Table of Contents

01. Css Loading Util
02. Theme Selector And Initializer
*/

/* 01. Css Loading Util */
function loadStyle(href, callback) {
  for (var i = 0; i < document.styleSheets.length; i++) {
    if (document.styleSheets[i].href == href) {
      return;
    }
  }
  var head = document.getElementsByTagName("head")[0];
  var link = document.createElement("link");
  link.rel = "stylesheet";
  link.type = "text/css";
  link.href = href;
  if (callback) {
    link.onload = function () {
      callback();
    };
  }
  var mainCss = $(head).find('[href$="main.css"]');
  if (mainCss.length !== 0) {
    mainCss[0].before(link);
  } else {
    head.appendChild(link);
  }
}

/* 02. Theme Selector, Layout Direction And Initializer */
(function ($) {
  if ($().dropzone) {
    Dropzone.autoDiscover = false;
  }

  var themeColorsDom =
    `<div class="theme-colors">
      <div class="p-4">
        <p class="text-muted mb-2">Light Theme</p>
        <div class="d-flex flex-row justify-content-between mb-4">
          <a href="#" data-theme="dore.light.blue.min.css" class="theme-color theme-color-blue"></a>
          <a href="#" data-theme="dore.light.purple.min.css" class="theme-color theme-color-purple"></a>
          <a href="#" data-theme="dore.light.green.min.css" class="theme-color theme-color-green"></a>
          <a href="#" data-theme="dore.light.orange.min.css" class="theme-color theme-color-orange"></a>
          <a href="#" data-theme="dore.light.red.min.css" class="theme-color theme-color-red"></a>
        </div>
        <p class="text-muted mb-2">Dark Theme</p>
        <div class="d-flex flex-row justify-content-between">
          <a href="#" data-theme="dore.dark.blue.min.css" class="theme-color theme-color-blue"></a>
          <a href="#" data-theme="dore.dark.purple.min.css" class="theme-color theme-color-purple"></a>
          <a href="#" data-theme="dore.dark.green.min.css" class="theme-color theme-color-green"></a>
          <a href="#" data-theme="dore.dark.orange.min.css" class="theme-color theme-color-orange"></a>
          <a href="#" data-theme="dore.dark.red.min.css" class="theme-color theme-color-red"></a>
        </div>
      </div>
      <div class="p-4">
        <p class="text-muted mb-2">Border Radius</p>
        <div class="custom-control custom-radio custom-control-inline">
          <input type="radio" id="roundedRadio" name="radiusRadio" class="custom-control-input radius-radio" data-radius="rounded">
          <label class="custom-control-label" for="roundedRadio">Rounded</label>
        </div>
        <div class="custom-control custom-radio custom-control-inline">
          <input type="radio" id="flatRadio" name="radiusRadio" class="custom-control-input radius-radio" data-radius="flat">
          <label class="custom-control-label" for="flatRadio">Flat</label>
        </div>
      </div>
      <div class="p-4">
        <p class="text-muted mb-2">Direction</p>
        <div class="custom-control custom-radio custom-control-inline">
          <input type="radio" id="ltrRadio" name="directionRadio" class="custom-control-input direction-radio" data-direction="ltr">
          <label class="custom-control-label" for="ltrRadio">Ltr</label>
        </div>
        <div class="custom-control custom-radio custom-control-inline">
          <input type="radio" id="rtlRadio" name="directionRadio" class="custom-control-input direction-radio" data-direction="rtl">
          <label class="custom-control-label" for="rtlRadio">Rtl</label>
        </div>
      </div>
      <a href="#" class="theme-button"> <i class="simple-icon-magic-wand"></i> </a> 
    </div>`;

  


  /* Default Theme Color, Border Radius and  Direction */
  var theme = "dore.light.blue.min.css";
  var direction = "ltr";
  var radius = "rounded";

  if (typeof Storage !== "undefined") {
    if (localStorage.getItem("dore-theme")) {
      theme = localStorage.getItem("dore-theme");
    } else {
      localStorage.setItem("dore-theme", theme);
    }
    if (localStorage.getItem("dore-direction")) {
      direction = localStorage.getItem("dore-direction");
    } else {
      localStorage.setItem("dore-direction", direction);
    }
    if (localStorage.getItem("dore-radius")) {
      radius = localStorage.getItem("dore-radius");
    } else {
      localStorage.setItem("dore-radius", radius);
    }
  }

  $(".theme-color[data-theme='" + theme + "']").addClass("active");
  $(".direction-radio[data-direction='" + direction + "']").attr("checked", true);
  $(".radius-radio[data-radius='" + radius + "']").attr("checked", true);
  $("#switchDark").attr("checked", theme.indexOf("dark") > 0 ? true : false);

  loadStyle("assets/css/" + theme, onStyleComplete);
  function onStyleComplete() {
    setTimeout(onStyleCompleteDelayed, 300);
  }

  function onStyleCompleteDelayed() {
    $("body").addClass(direction);
    $("html").attr("dir", direction);
    $("body").addClass(radius);
    $("body").dore();
  }

  $("body").on("click", ".theme-color", function (event) {
    event.preventDefault();
    var dataTheme = $(this).data("theme");
    if (typeof Storage !== "undefined") {
      localStorage.setItem("dore-theme", dataTheme);
      window.location.reload();
    }
  });

  $("input[name='directionRadio']").on("change", function (event) {
    var direction = $(event.currentTarget).data("direction");
    if (typeof Storage !== "undefined") {
      localStorage.setItem("dore-direction", direction);
      window.location.reload();
    }
  });

  $("input[name='radiusRadio']").on("change", function (event) {
    var radius = $(event.currentTarget).data("radius");
    if (typeof Storage !== "undefined") {
      localStorage.setItem("dore-radius", radius);
      window.location.reload();
    }
  });

  $("#switchDark").on("change", function (event) {
    var mode = $(event.currentTarget)[0].checked ? "dark" : "light";
    if (mode == "dark") {
      theme = theme.replace("light", "dark");
    } else if (mode == "light") {
      theme = theme.replace("dark", "light");
    }
    if (typeof Storage !== "undefined") {
      localStorage.setItem("dore-theme", theme);
      window.location.reload();
    }
  });

  $(".theme-button").on("click", function (event) {
    event.preventDefault();
    $(this)
      .parents(".theme-colors")
      .toggleClass("shown");
  });

  $(document).on("click", function (event) {
    if (
      !(
        $(event.target)
          .parents()
          .hasClass("theme-colors") ||
        $(event.target)
          .parents()
          .hasClass("theme-button") ||
        $(event.target).hasClass("theme-button") ||
        $(event.target).hasClass("theme-colors")
      )
    ) {
      if ($(".theme-colors").hasClass("shown")) {
        $(".theme-colors").removeClass("shown");
      }
    }
  });
})(jQuery);


/*
  Added JavaScript
  01. Staff DataTables
  02. Baki DataTables
  03. Pabrikan DataTables
  04. Sell Transaction DataTables
  05. Buy Transaction DataTables
  06. Trade Transaction DataTables
  07. Update Harga Form
*/

// 01. Staff DataTables

var $dataTableRows = $("#staffDataTable").DataTable({
        bLengthChange: false,
        destroy: true,
        info: false,
        sDom: '<"row view-filter"<"col-sm-12"<"float-left"l><"float-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
        pageLength: 10,
        columns: [
          { data: "Code" },
          { data: "Name" },
          { data: "Job" }
        ],
        language: {
          paginate: {
            previous: "<i class='simple-icon-arrow-left'></i>",
            next: "<i class='simple-icon-arrow-right'></i>"
          }
        },
        drawCallback: function () {
          unCheckAllRows();
          $("#checkAllDataTables").prop("checked", false);
          $("#checkAllDataTables").prop("indeterminate", false).trigger("change");

          $($(".dataTables_wrapper .pagination li:first-of-type"))
            .find("a")
            .addClass("prev");
          $($(".dataTables_wrapper .pagination li:last-of-type"))
            .find("a")
            .addClass("next");
          $(".dataTables_wrapper .pagination").addClass("pagination-sm");
          var api = $(this).dataTable().api();
          $("#pageCountDatatable span").html("Displaying " + parseInt(api.page.info().start + 1) + "-" + api.page.info().end + " of " + api.page.info().recordsTotal + " items");
        }
      });

      $('#staffDataTable tbody').on( 'click', 'tr', function () {
        $(this).toggleClass('selected');
        var $checkBox = $(this).find(".custom-checkbox input");
        $checkBox.prop("checked", !$checkBox.prop("checked")).trigger("change");
        controlCheckAll();
      });

      function controlCheckAll() {
        var anyChecked = false;
        var allChecked = true;
        $('#staffDataTable tbody tr .custom-checkbox input').each(function () {
          if ($(this).prop("checked")) {
            anyChecked = true;
          } else {
            allChecked = false;
          }
        });
        if (anyChecked) {
          $("#checkAllDataTables").prop("indeterminate", anyChecked);
        } else {
          $("#checkAllDataTables").prop("indeterminate", anyChecked);
          $("#checkAllDataTables").prop("checked", anyChecked);
        }
        if (allChecked) {
          $("#checkAllDataTables").prop("indeterminate", false);
          $("#checkAllDataTables").prop("checked", allChecked);
        }
      }

      function unCheckAllRows () {
        $('#staffDataTable tbody tr').removeClass('selected');
        $('#staffDataTable tbody tr .custom-checkbox input').prop("checked", false).trigger("change");
      }

      function checkAllRows () {
        $('#staffDataTable tbody tr').addClass('selected');
        $('#staffDataTable tbody tr .custom-checkbox input').prop("checked", true).trigger("change");
      }

      $("#checkAllDataTables").on("click", function(event) {
        var isCheckedAll = $("#checkAllDataTables").prop("checked");
        if(isCheckedAll) {
          checkAllRows();
        } else {
          unCheckAllRows();
        }
      });

      function getSelectedRows() {
        //Getting Selected Ones
        console.log($dataTableRows.rows('.selected').data());
        
      }

      $("#searchDatatable").on("keyup", function (event) {
        $dataTableRows.search($(this).val()).draw();
      });

      $("#pageCountDatatable .dropdown-menu a").on("click", function (event) {
        var selText = $(this).text();
        $dataTableRows.page.len(parseInt(selText)).draw();
      });

      var $addToDatatableButton = $("#addToDatatable").stateButton();

      // Validation when modal shown
      $('#rightModal').on('shown.bs.modal', function (e) {
        $("#addToDatatableForm").validate(
          {
            rules: {
              Code: {
                required: true,
              },
              Name: {
                required: true,
              },
              Job: {
                required: true
              }
            }
          }
        )
      })


// 02. Baki DataTables
var $dataTableRows = $("#bakiDataTable").DataTable({
        bLengthChange: false,
        destroy: true,
        info: false,
        sDom: '<"row view-filter"<"col-sm-12"<"float-left"l><"float-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
        pageLength: 10,
        columns: [
          { data: "Code" },
          { data: "Name" }
        ],
        language: {
          paginate: {
            previous: "<i class='simple-icon-arrow-left'></i>",
            next: "<i class='simple-icon-arrow-right'></i>"
          }
        },
        drawCallback: function () {
          unCheckAllRows();
          $("#checkAllDataTables").prop("checked", false);
          $("#checkAllDataTables").prop("indeterminate", false).trigger("change");

          $($(".dataTables_wrapper .pagination li:first-of-type"))
            .find("a")
            .addClass("prev");
          $($(".dataTables_wrapper .pagination li:last-of-type"))
            .find("a")
            .addClass("next");
          $(".dataTables_wrapper .pagination").addClass("pagination-sm");
          var api = $(this).dataTable().api();
          $("#pageCountDatatable span").html("Displaying " + parseInt(api.page.info().start + 1) + "-" + api.page.info().end + " of " + api.page.info().recordsTotal + " items");
        }
      });

      $('#bakiDataTable tbody').on( 'click', 'tr', function () {
        $(this).toggleClass('selected');
        var $checkBox = $(this).find(".custom-checkbox input");
        $checkBox.prop("checked", !$checkBox.prop("checked")).trigger("change");
        controlCheckAll();
      });

      function controlCheckAll() {
        var anyChecked = false;
        var allChecked = true;
        $('#bakiDataTable tbody tr .custom-checkbox input').each(function () {
          if ($(this).prop("checked")) {
            anyChecked = true;
          } else {
            allChecked = false;
          }
        });
        if (anyChecked) {
          $("#checkAllDataTables").prop("indeterminate", anyChecked);
        } else {
          $("#checkAllDataTables").prop("indeterminate", anyChecked);
          $("#checkAllDataTables").prop("checked", anyChecked);
        }
        if (allChecked) {
          $("#checkAllDataTables").prop("indeterminate", false);
          $("#checkAllDataTables").prop("checked", allChecked);
        }
      }

      function unCheckAllRows () {
        $('#bakiDataTable tbody tr').removeClass('selected');
        $('#bakiDataTable tbody tr .custom-checkbox input').prop("checked", false).trigger("change");
      }

      function checkAllRows () {
        $('#bakiDataTable tbody tr').addClass('selected');
        $('#bakiDataTable tbody tr .custom-checkbox input').prop("checked", true).trigger("change");
      }

      $("#checkAllDataTables").on("click", function(event) {
        var isCheckedAll = $("#checkAllDataTables").prop("checked");
        if(isCheckedAll) {
          checkAllRows();
        } else {
          unCheckAllRows();
        }
      });

      function getSelectedRows() {
        //Getting Selected Ones
        console.log($dataTableRows.rows('.selected').data());
      }

      $("#searchDatatable").on("keyup", function (event) {
        $dataTableRows.search($(this).val()).draw();
      });

      $("#pageCountDatatable .dropdown-menu a").on("click", function (event) {
        var selText = $(this).text();
        $dataTableRows.page.len(parseInt(selText)).draw();
      });

      var $addToDatatableButton = $("#addToDatatable").stateButton();

      // Validation when modal shown
      $('#rightModal').on('shown.bs.modal', function (e) {
        $("#addToDatatableForm").validate(
          {
            rules: {
              
              Name: {
                required: true,
              }
              
            }
          }
        )
      })

// 03. Pabrikan DataTables
var $dataTableRows = $("#pabrikanDataTable").DataTable({
        bLengthChange: false,
        destroy: true,
        info: false,
        sDom: '<"row view-filter"<"col-sm-12"<"float-left"l><"float-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
        pageLength: 10,
        columns: [
          { data: "Code" },
          { data: "Name" }
        ],
        language: {
          paginate: {
            previous: "<i class='simple-icon-arrow-left'></i>",
            next: "<i class='simple-icon-arrow-right'></i>"
          }
        },
        drawCallback: function () {
          unCheckAllRows();
          $("#checkAllDataTables").prop("checked", false);
          $("#checkAllDataTables").prop("indeterminate", false).trigger("change");

          $($(".dataTables_wrapper .pagination li:first-of-type"))
            .find("a")
            .addClass("prev");
          $($(".dataTables_wrapper .pagination li:last-of-type"))
            .find("a")
            .addClass("next");
          $(".dataTables_wrapper .pagination").addClass("pagination-sm");
          var api = $(this).dataTable().api();
          $("#pageCountDatatable span").html("Displaying " + parseInt(api.page.info().start + 1) + "-" + api.page.info().end + " of " + api.page.info().recordsTotal + " items");
        }
      });

      $('#pabrikanDataTable tbody').on( 'click', 'tr', function () {
        $(this).toggleClass('selected');
        var $checkBox = $(this).find(".custom-checkbox input");
        $checkBox.prop("checked", !$checkBox.prop("checked")).trigger("change");
        controlCheckAll();
      });

      function controlCheckAll() {
        var anyChecked = false;
        var allChecked = true;
        $('#pabrikanDataTable tbody tr .custom-checkbox input').each(function () {
          if ($(this).prop("checked")) {
            anyChecked = true;
          } else {
            allChecked = false;
          }
        });
        if (anyChecked) {
          $("#checkAllDataTables").prop("indeterminate", anyChecked);
        } else {
          $("#checkAllDataTables").prop("indeterminate", anyChecked);
          $("#checkAllDataTables").prop("checked", anyChecked);
        }
        if (allChecked) {
          $("#checkAllDataTables").prop("indeterminate", false);
          $("#checkAllDataTables").prop("checked", allChecked);
        }
      }

      function unCheckAllRows () {
        $('#pabrikanDataTable tbody tr').removeClass('selected');
        $('#pabrikanDataTable tbody tr .custom-checkbox input').prop("checked", false).trigger("change");
      }

      function checkAllRows () {
        $('#pabrikanDataTable tbody tr').addClass('selected');
        $('#pabrikanDataTable tbody tr .custom-checkbox input').prop("checked", true).trigger("change");
      }

      $("#checkAllDataTables").on("click", function(event) {
        var isCheckedAll = $("#checkAllDataTables").prop("checked");
        if(isCheckedAll) {
          checkAllRows();
        } else {
          unCheckAllRows();
        }
      });

      function getSelectedRows() {
        //Getting Selected Ones
        console.log($dataTableRows.rows('.selected').data());
      }

      $("#searchDatatable").on("keyup", function (event) {
        $dataTableRows.search($(this).val()).draw();
      });

      $("#pageCountDatatable .dropdown-menu a").on("click", function (event) {
        var selText = $(this).text();
        $dataTableRows.page.len(parseInt(selText)).draw();
      });

      var $addToDatatableButton = $("#addToDatatable").stateButton();

      // Validation when modal shown
      $('#rightModal').on('shown.bs.modal', function (e) {
        $("#addToDatatableForm").validate(
          {
            rules: {
              Code: {
                required: true,
              },
              Name: {
                required: true,
              },
              Job: {
                required: true
              }
            }
          }
        )
      })


// 04. Sell Transaction DataTables
var $dataTableRows = $("#sellDataTable").DataTable({
        bLengthChange: false,
        destroy: true,
        info: false,
        sDom: '<"row view-filter"<"col-sm-12"<"float-left"l><"float-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
        pageLength: 10,
        columns: [
          { data: "Kode Transaksi" },
          { data: "Tanggal" },
          { data: "Kode Barang" },
          { data: "Kode Jenis" },
          { data: "Name" },
          { data: "Berat" },
          { data: "Kadar" },
          { data: "Warna" },
          { data: "Baki" },
          { data: "Pabrikan" },
          { data: "Nama Baki" },
          { data: "Jenis Barang" },
          { data: "Kelas Barang" },
          { data: "Berat Batu" },
          { data: "Ukuran" }

        ],
        language: {
          paginate: {
            previous: "<i class='simple-icon-arrow-left'></i>",
            next: "<i class='simple-icon-arrow-right'></i>"
          }
        },
        drawCallback: function () {
          unCheckAllRows();
          $("#checkAllDataTables").prop("checked", false);
          $("#checkAllDataTables").prop("indeterminate", false).trigger("change");

          $($(".dataTables_wrapper .pagination li:first-of-type"))
            .find("a")
            .addClass("prev");
          $($(".dataTables_wrapper .pagination li:last-of-type"))
            .find("a")
            .addClass("next");
          $(".dataTables_wrapper .pagination").addClass("pagination-sm");
          var api = $(this).dataTable().api();
          $("#pageCountDatatable span").html("Displaying " + parseInt(api.page.info().start + 1) + "-" + api.page.info().end + " of " + api.page.info().recordsTotal + " items");
        }
      });

      $('#sellDataTable tbody').on( 'click', 'tr', function () {
        $(this).toggleClass('selected');
        var $checkBox = $(this).find(".custom-checkbox input");
        $checkBox.prop("checked", !$checkBox.prop("checked")).trigger("change");
        controlCheckAll();
      });

      function controlCheckAll() {
        var anyChecked = false;
        var allChecked = true;
        $('#sellDataTable tbody tr .custom-checkbox input').each(function () {
          if ($(this).prop("checked")) {
            anyChecked = true;
          } else {
            allChecked = false;
          }
        });
        if (anyChecked) {
          $("#checkAllDataTables").prop("indeterminate", anyChecked);
        } else {
          $("#checkAllDataTables").prop("indeterminate", anyChecked);
          $("#checkAllDataTables").prop("checked", anyChecked);
        }
        if (allChecked) {
          $("#checkAllDataTables").prop("indeterminate", false);
          $("#checkAllDataTables").prop("checked", allChecked);
        }
      }

      function unCheckAllRows () {
        $('#sellDataTable tbody tr').removeClass('selected');
        $('#sellDataTable tbody tr .custom-checkbox input').prop("checked", false).trigger("change");
      }

      function checkAllRows () {
        $('#sellDataTable tbody tr').addClass('selected');
        $('#sellDataTable tbody tr .custom-checkbox input').prop("checked", true).trigger("change");
      }

      $("#checkAllDataTables").on("click", function(event) {
        var isCheckedAll = $("#checkAllDataTables").prop("checked");
        if(isCheckedAll) {
          checkAllRows();
        } else {
          unCheckAllRows();
        }
      });

      function getSelectedRows() {
        //Getting Selected Ones
        console.log($dataTableRows.rows('.selected').data());
      }

      $("#searchDatatable").on("keyup", function (event) {
        $dataTableRows.search($(this).val()).draw();
      });

      $("#pageCountDatatable .dropdown-menu a").on("click", function (event) {
        var selText = $(this).text();
        $dataTableRows.page.len(parseInt(selText)).draw();
      });

      var $addToDatatableButton = $("#addToDatatable").stateButton();

      // Validation when modal shown
      $('#rightModal').on('shown.bs.modal', function (e) {
        $("#addToDatatableForm").validate(
          {
            rules: {
              Code: {
                required: true,
              },
              Name: {
                required: true,
              },
              Job: {
                required: true
              }
            }
          }
        )
      })

// 05. Buy Transaction DataTables
var $dataTableRows = $("#buyDataTable").DataTable({
        bLengthChange: false,
        destroy: true,
        info: false,
        sDom: '<"row view-filter"<"col-sm-12"<"float-left"l><"float-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
        pageLength: 10,
        columns: [
          { data: "Kode Transaksi" },
          { data: "Tanggal" },
          { data: "Kode Barang" },
          { data: "Kode Jenis" },
          { data: "Name" },
          { data: "Berat" },
          { data: "Kadar" },
          { data: "Warna" },
          { data: "Baki" },
          { data: "Pabrikan" },
          { data: "Nama Baki" },
          { data: "Jenis Barang" },
          { data: "Kelas Barang" },
          { data: "Berat Batu" },
          { data: "Ukuran" }

        ],
        language: {
          paginate: {
            previous: "<i class='simple-icon-arrow-left'></i>",
            next: "<i class='simple-icon-arrow-right'></i>"
          }
        },
        drawCallback: function () {
          unCheckAllRows();
          $("#checkAllDataTables").prop("checked", false);
          $("#checkAllDataTables").prop("indeterminate", false).trigger("change");

          $($(".dataTables_wrapper .pagination li:first-of-type"))
            .find("a")
            .addClass("prev");
          $($(".dataTables_wrapper .pagination li:last-of-type"))
            .find("a")
            .addClass("next");
          $(".dataTables_wrapper .pagination").addClass("pagination-sm");
          var api = $(this).dataTable().api();
          $("#pageCountDatatable span").html("Displaying " + parseInt(api.page.info().start + 1) + "-" + api.page.info().end + " of " + api.page.info().recordsTotal + " items");
        }
      });

      $('#buyDataTable tbody').on( 'click', 'tr', function () {
        $(this).toggleClass('selected');
        var $checkBox = $(this).find(".custom-checkbox input");
        $checkBox.prop("checked", !$checkBox.prop("checked")).trigger("change");
        controlCheckAll();
      });

      function controlCheckAll() {
        var anyChecked = false;
        var allChecked = true;
        $('#buyDataTable tbody tr .custom-checkbox input').each(function () {
          if ($(this).prop("checked")) {
            anyChecked = true;
          } else {
            allChecked = false;
          }
        });
        if (anyChecked) {
          $("#checkAllDataTables").prop("indeterminate", anyChecked);
        } else {
          $("#checkAllDataTables").prop("indeterminate", anyChecked);
          $("#checkAllDataTables").prop("checked", anyChecked);
        }
        if (allChecked) {
          $("#checkAllDataTables").prop("indeterminate", false);
          $("#checkAllDataTables").prop("checked", allChecked);
        }
      }

      function unCheckAllRows () {
        $('#buyDataTable tbody tr').removeClass('selected');
        $('#buyDataTable tbody tr .custom-checkbox input').prop("checked", false).trigger("change");
      }

      function checkAllRows () {
        $('#buyDataTable tbody tr').addClass('selected');
        $('#buyDataTable tbody tr .custom-checkbox input').prop("checked", true).trigger("change");
      }

      $("#checkAllDataTables").on("click", function(event) {
        var isCheckedAll = $("#checkAllDataTables").prop("checked");
        if(isCheckedAll) {
          checkAllRows();
        } else {
          unCheckAllRows();
        }
      });

      function getSelectedRows() {
        //Getting Selected Ones
        console.log($dataTableRows.rows('.selected').data());
      }

      $("#searchDatatable").on("keyup", function (event) {
        $dataTableRows.search($(this).val()).draw();
      });

      $("#pageCountDatatable .dropdown-menu a").on("click", function (event) {
        var selText = $(this).text();
        $dataTableRows.page.len(parseInt(selText)).draw();
      });

      var $addToDatatableButton = $("#addToDatatable").stateButton();

      // Validation when modal shown
      $('#rightModal').on('shown.bs.modal', function (e) {
        $("#addToDatatableForm").validate(
          {
            rules: {
              Code: {
                required: true,
              },
              Name: {
                required: true,
              },
              Job: {
                required: true
              }
            }
          }
        )
      })

// 06. Trade Transaction DataTables
var $dataTableRows = $("#tradeDataTable").DataTable({
        bLengthChange: false,
        destroy: true,
        scrollX: true,
        sScrollXInner: "100%",
        info: false,
        sDom: '<"row view-filter"<"col-sm-12"<"float-left"l><"float-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
        pageLength: 10,
        columns: [
          { data: "suratGadai" },
          { data: "Tanggalgadai" },
          { data: "Name" },
          { data: "Alamat" },
          { data: "HP" },
          { data: "Sales" },
          { data: "Kasir" },
          { data: "Pinjaman" },
          { data: "Namabarangtitip" },
          { data: "berat" },
          { data: "kadar" },
          { data: "batu" },
          { data: "Namatokosurat" },
          { data: "tanggalbayar" },
          { data: "bunga" },
          { data: "tanggalambil" },
          { data: "salesambil" },
          { data: "kasirambil" },
          { data: "keterangan" }

        ],
        language: {
          paginate: {
            previous: "<i class='simple-icon-arrow-left'></i>",
            next: "<i class='simple-icon-arrow-right'></i>"
          }
        },
        drawCallback: function () {
          unCheckAllRows();
          $("#checkAllDataTables").prop("checked", false);
          $("#checkAllDataTables").prop("indeterminate", false).trigger("change");

          $($(".dataTables_wrapper .pagination li:first-of-type"))
            .find("a")
            .addClass("prev");
          $($(".dataTables_wrapper .pagination li:last-of-type"))
            .find("a")
            .addClass("next");
          $(".dataTables_wrapper .pagination").addClass("pagination-sm");
          var api = $(this).dataTable().api();
          $("#pageCountDatatable span").html("Displaying " + parseInt(api.page.info().start + 1) + "-" + api.page.info().end + " of " + api.page.info().recordsTotal + " items");
        }
      });

      $('#tradeDataTable tbody').on( 'click', 'tr', function () {
        $(this).toggleClass('selected');
        var $checkBox = $(this).find(".custom-checkbox input");
        $checkBox.prop("checked", !$checkBox.prop("checked")).trigger("change");
        controlCheckAll();
      });

      function controlCheckAll() {
        var anyChecked = false;
        var allChecked = true;
        $('#tradeDataTable tbody tr .custom-checkbox input').each(function () {
          if ($(this).prop("checked")) {
            anyChecked = true;
          } else {
            allChecked = false;
          }
        });
        if (anyChecked) {
          $("#checkAllDataTables").prop("indeterminate", anyChecked);
        } else {
          $("#checkAllDataTables").prop("indeterminate", anyChecked);
          $("#checkAllDataTables").prop("checked", anyChecked);
        }
        if (allChecked) {
          $("#checkAllDataTables").prop("indeterminate", false);
          $("#checkAllDataTables").prop("checked", allChecked);
        }
      }

      function unCheckAllRows () {
        $('#tradeDataTable tbody tr').removeClass('selected');
        $('#tradeDataTable tbody tr .custom-checkbox input').prop("checked", false).trigger("change");
      }

      function checkAllRows () {
        $('#tradeDataTable tbody tr').addClass('selected');
        $('#tradeDataTable tbody tr .custom-checkbox input').prop("checked", true).trigger("change");
      }

      $("#checkAllDataTables").on("click", function(event) {
        var isCheckedAll = $("#checkAllDataTables").prop("checked");
        if(isCheckedAll) {
          checkAllRows();
        } else {
          unCheckAllRows();
        }
      });

      function getSelectedRows() {
        //Getting Selected Ones
        console.log($dataTableRows.rows('.selected').data());
      }

      $("#searchDatatable").on("keyup", function (event) {
        $dataTableRows.search($(this).val()).draw();
      });

      $("#pageCountDatatable .dropdown-menu a").on("click", function (event) {
        var selText = $(this).text();
        $dataTableRows.page.len(parseInt(selText)).draw();
      });

      var $addToDatatableButton = $("#addToDatatable").stateButton();

      // Validation when modal shown
      $('#rightModal').on('shown.bs.modal', function (e) {
        $("#addToDatatableForm").validate(
          {
            rules: {
              Code: {
                required: true,
              },
              Name: {
                required: true,
              },
              Job: {
                required: true
              }
            }
          }
        )
      })
// 07. Update Harga Form
      $("#hk375").change(function(){
        var hdasar = $("#hk375").val();
        var hrp = Number(hdasar)+Number(10000);
        var hh = Number(hdasar)+Number(15000);
        var hs = Number(hdasar)+Number(20000);
        $("#hrp375").val(hrp);
        $("#hh375").val(hh);
        $("#hs375").val(hs);
      })
      $("#hk375kl").change(function(){
        var hdasar = $("#hk375kl").val();
        var hrp = Number(hdasar)+Number(10000);
        var hh = Number(hdasar)+Number(15000);
        var hs = Number(hdasar)+Number(20000);
        $("#hrp375kl").val(hrp);
        $("#hh375kl").val(hh);
        $("#hs375kl").val(hs);
      })
      $("#hk400").change(function(){
        var hdasar = $("#hk400").val();
        var hrp = Number(hdasar)+Number(10000);
        var hh = Number(hdasar)+Number(15000);
        var hs = Number(hdasar)+Number(20000);
        $("#hrp400").val(hrp);
        $("#hh400").val(hh);
        $("#hs400").val(hs);
      })
      $("#hk420").change(function(){
        var hdasar = $("#hk420").val();
        var hrp = Number(hdasar)+Number(10000);
        var hh = Number(hdasar)+Number(15000);
        var hs = Number(hdasar)+Number(20000);
        $("#hrp420").val(hrp);
        $("#hh420").val(hh);
        $("#hs420").val(hs);
      })
      $("#hk700").change(function(){
        var hdasar = $("#hk700").val();
        var hrp = Number(hdasar)+Number(10000);
        var hh = Number(hdasar)+Number(15000);
        var hs = Number(hdasar)+Number(20000);
        $("#hrp700").val(hrp);
        $("#hh700").val(hh);
        $("#hs700").val(hs);
      })
      $("#hk700kl").change(function(){
        var hdasar = $("#hk700kl").val();
        var hrp = Number(hdasar)+Number(10000);
        var hh = Number(hdasar)+Number(15000);
        var hs = Number(hdasar)+Number(20000);
        $("#hrp700kl").val(hrp);
        $("#hh700kl").val(hh);
        $("#hs700kl").val(hs);
      })
      $("#hk750").change(function(){
        var hdasar = $("#hk750").val();
        var hrp = Number(hdasar)+Number(10000);
        var hh = Number(hdasar)+Number(15000);
        var hs = Number(hdasar)+Number(20000);
        $("#hrp750").val(hrp);
        $("#hh750").val(hh);
        $("#hs750").val(hs);
      })
      $("#hk750kl").change(function(){
        var hdasar = $("#hk750kl").val();
        var hrp = Number(hdasar)+Number(10000);
        var hh = Number(hdasar)+Number(15000);
        var hs = Number(hdasar)+Number(20000);
        $("#hrp750kl").val(hrp);
        $("#hh750kl").val(hh);
        $("#hs750kl").val(hs);
      })
// 08. Automatic Price Update

//CAMERA
  window.addEventListener("load", function(){
  // [1] GET ALL THE HTML ELEMENTS
  var video = document.getElementById("vid-show"),
      canvas = document.getElementById("vid-canvas"),
      take = document.getElementById("vid-take");

  // [2] ASK FOR USER PERMISSION TO ACCESS CAMERA
  // WILL FAIL IF NO CAMERA IS ATTACHED TO COMPUTER
  navigator.mediaDevices.getUserMedia({ video : true })
  .then(function(stream) {
    // [3] SHOW VIDEO STREAM ON VIDEO TAG
    video.srcObject = stream;
    video.play();

    // [4] WHEN WE CLICK ON "TAKE PHOTO" BUTTON
    take.addEventListener("click", function(){
      // Create snapshot from video
      var draw = document.createElement("canvas");
      draw.width = video.videoWidth;
      draw.height = video.videoHeight;
      var context2D = draw.getContext("2d");
      context2D.drawImage(video, 0, 0, video.videoWidth, video.videoHeight);
      // Put into canvas container
      canvas.innerHTML = "";
      canvas.appendChild(draw);
    });
  })
  .catch(function(err) {
    document.getElementById("vid-controls").innerHTML = "Please enable access and attach a camera";
  });
});