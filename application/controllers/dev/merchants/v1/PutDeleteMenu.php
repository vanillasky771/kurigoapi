<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class putDeleteMenu extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_put() {
        $merchantItemId = $this->put('id');
        $token = $this->put('token');
        $checker = $this->db->get_where('merchant_credential', array('token' => $token))->result();
        if (count($checker) == 1 ){  
            $deleteStatus = $this->db->delete('merchant_item', array('item_id' => $merchantItemId)); 
            if ($deleteStatus) {
                $this->sentResponse("Data", "", "Merchant Item Deleted", 200, array($token, $merchantItemId));
            } else {
                $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401, array($token, $merchantItemId));
            }
        }
        
    }
}