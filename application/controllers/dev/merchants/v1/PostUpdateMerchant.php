<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class postUpdateMerchant extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_post() {
        $token = $this->post('token');
        $merchantId = $this->post('merchantId');
        $merchantName = $this->post('merchantName');
        $merchantDisplayName = $this->post('merchantDisplayNme');
        $merchantAddress = $this->post('merchantAddress');
        $merchantLat = $this->post('merchantLat');
        $merchantLong = $this->post('merchantLong');
        $merchantPhone = $this->post('merchantPhone');
        $merchantImage = $this->post('merchantImagePath');
        $merchantCity = $this->post('merchantCity');
        $merchantStatus = $this->post('merchantStatus');
        $checker = $this->db->get_where('credential', array('token' => $token))->result();
        if (count($checker) == 1 ){
            if ($merchantImage != "") {
                define('UPLOAD_DIR', "merchantImage/"); //local
                $img = str_replace(' ', '+', $merchantImage);
                $data = base64_decode($img);
                $file = UPLOAD_DIR .$merchantName.'.png';
                $success=file_put_contents($file, $data);
                if ($success) {
                    $updateImage = "merchantImage/".$merchantName.".png";
                } else {
                    $updateImage = $merchantImage;
                }
            }
            
            $merchantUpdateData = array(
                'merchant_name'        => $merchantName,
                'merchant_displayName' => $merchantDisplayName,
                'merchant_address'     => $merchantAddress,
                'merchant_lat'         => $merchantLat,
                'merchant_long'        => $merchantLong,
                'merchant_phone'       => $merchantPhone,
                'merchant_image'       => $updateImage,
                'merchant_city'        => $merchantCity,
                'merchant_status'      => $merchantStatus
            );
            $this->db->where('merchant_id', $merchantId);
            $updateResult = $this->db->update('merchant', $merchantUpdateData);
            if ($updateResult){
                $this->sentResponse("Data", $merchantUpdateData, "Menu Updated", 200,$merchantUpdateData);
            } else {
                $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401, $merchantUpdateData);
            }
        } else {
            $this->sendErrorResponse("Oops Credential not valid, please login again!.", 10001, "", 402, $token);
        } 
    }
}