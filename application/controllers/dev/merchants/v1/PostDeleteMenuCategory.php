<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class postDeleteMenuCategory extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_post() {
        $categoryId = $this->post('menuCategoryId');
        $token = $this->post('token');
        $checker = $this->db->get_where('merchant_credential', array('token' => $token))->result();
        if (count($checker) == 1 ){
            $deleteStatus = $this->db->delete('merchant_itemCategory', array('merchcategory_id' => $categoryId)); 
            if ($deleteStatus) {
                $this->sentResponse("Data", "", "Category Deleted", 200, array($token, $categoryId));
            } else {
                $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401, array($token, $categoryId));
            }
        } else {
            $this->sendErrorResponse("Oops Credential not valid, please login again!.", 10001, "", 402, $token);
        }
    }
}