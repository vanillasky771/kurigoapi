<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class getMerchantList extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_get() {
        $token = $this->get('token');
        $checker = $this->db->get_where('credential', array('token' => $token))->result();
        if (count($checker) == 1 ){
            $merchantList = $this->db->get('merchant')->result();
            if (!empty($merchantList)) {
                $this->sentResponse("MerchantList", $merchantList, "", 200,$token);
            } else if (empty($merchantList)) {
                $this->sentResponse("MerchantList", [], "", 200,$token);
            } else {
                $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401, $token);
            }
        } else {
            $this->sendErrorResponse("Oops Credential not valid, please login again!.", 10001, "", 402, $token);
        }
    }
}