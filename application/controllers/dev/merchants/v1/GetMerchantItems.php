<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class getMerchantItems extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_get() {
        $token = $this->get('token');
        $merchantId = $this->get('merchId');
        $checker = $this->db->get_where('credential', array('token' => $token))->result();
        if (count($checker) == 1 ){
            $merchantItemData = $this->db->get_where('merchant_item', array('merchant_id' => $merchantId))->result();
            if (!empty($merchantItemData)) {
                foreach($merchantItemData as $result){
                    $result->item_availability = (bool) $result->item_availability;
                }
                $this->sentResponse("MerchantItemList", $merchantItemData, "", 200, array($token, $merchantId));
            } else if (empty($merchantItemData)) {
                $this->sentResponse("MerchantItemList", [], "", 200, array($token, $merchantId));
            } else {
                $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401, array($token, $merchantId));
            }
        } else {
            $this->sendErrorResponse("Oops Credential not valid, please login again!.", 10001, "", 402, $token);
        }
    }
}