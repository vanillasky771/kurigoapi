<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class postAddFoodCategory extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_post() {
        $merchantId = $this->post('merchantID');
        $categoryName = $this->post('categoryName');
        $categoryDesc = $this->post('categoryDesc');
        $token = $this->post('token');
        
        $checker = $this->db->get_where('merchant_credential', array('token' => $token))->result();
        if (count($checker) == 1 ){
            $categoryData = array(
                'merchant_id' => $merchantId,
                'category_name' => $categoryName,
                'category_description' => $categoryDesc
            );
            $insertStatus = $this->db->insert('merchant_itemCategory', $categoryData);
            if ($insertStatus) {
                $this->sentResponse("Data", $categoryData, "Menu Category Added", 200,$categoryData);
            } else {
                $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401, $categoryData);
            }
        } else {
            $this->sendErrorResponse("Oops Credential not valid, please login again!.", 10001, "", 402, $token);
        }
    }
}