<?php
use chriskacerguis\RestServer\RestController;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/RestController.php';
require APPPATH . 'libraries/Format.php';

class getItemCategory extends RestController {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_get() {
        $token = $this->get('token');
        $type = $this->get('type');
        $checker = $this->db->get_where('credential', array('token' => $token))->result();
        if (count($checker) == 1 ){
            $this->db->where('category_class', $type);
            $itemCategoryData = $this->db->get('item_category')->result();
            if (!empty($itemCategoryData)) {
                $response['CategoryList'] = $itemCategoryData;
                $this->response($response);
            } else if (empty($itemCategoryData)) {
                $response =  [];
                $this->response($response, 200);
            } else {
                $response['Title'] =  "Oops something wrong, try again later!.";
                $response['Code'] = 10004;
                $response['Message'] = "";
                $this->response($response, 401);
            }
        } else {
            $response['Title'] =  "Oops Credential not valid, please login again!.";
            $response['Code'] = 10001;
            $response['Message'] = "";
            $this->response($response, 402);
        }
    }
}