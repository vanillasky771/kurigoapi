<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class postAddMerchMenu extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_post() {
        $token = $this->post('token');
        $merchantId = $this->post('merchantId');
        $itemName = $this->post('itemName');
        $itemDesc = $this->post('itemDesc');
        $itemPrice = $this->post('itemPrice');
        $itemAvail = $this->post('itemAvail');
        $itemStock = $this->post('itemStock');
        $itemAddon = $this->post('itemAddOn');
        $itemWeight = $this->post('itemWeight');
        $itemPhotos = $this->post('photosUrl');
        $itemCategory = $this->post('itemCategory');
        $itemSubCategory = $this->post('itemSubCategory');
        
        $checker = $this->db->get_where('merchant_credential', array('token' => $token))->result();
        if (count($checker) == 1 ){
            define('UPLOAD_DIR', "merchantMenuImage/"); //local
            $img = str_replace(' ', '+', $itemPhotos);
            $data = base64_decode($img);
            $file = UPLOAD_DIR . $merchantId."_".$itemName.'.png';
            $success=file_put_contents($file, $data);
            if ($success) {
                $updateImage = "merchantMenuImage/".$merchantId."_".$itemName.".png";
            } else {
                $updateImage = $itemPhotos;
            }
            $merchantItemAddData = array(
                'merchant_id'  => $merchantId,
                'item_name'     => $itemName,
                'item_desc'    => $itemDesc,
                'item_price'    => $itemPrice,
                'item_availability' => $itemAvail,
                'item_addOn' => $itemAddon,
                'item_stock'    => $itemStock,
                'item_weight'   => $itemWeight,
                'item_photos'  => $updateImage,
                'item_category' => $itemCategory,
                'item_subcategory' => $itemSubCategory
            );
            $addResult = $this->db->insert('merchant_item', $merchantItemAddData);
            if ($addResult){
                $this->sentResponse("Data", $merchantItemAddData, "Menu Added", 200,$merchantItemAddData);
            } else {
                $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401, $merchantItemAddData);
            }
        } else {
            $this->sendErrorResponse("Oops Credential not valid, please login again!.", 10001, "", 402, $token);
        }   
    }
}