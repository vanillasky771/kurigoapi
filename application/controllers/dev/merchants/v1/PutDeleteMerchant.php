<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class putDeleteMerchant extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_put() {
        $merchantId = $this->put('id');
        $token = $this->put('token');
        $checker = $this->db->get_where('credential', array('token' => $token))->result();
        if (count($checker) == 1 ){  
            $deleteStatus = $this->db->delete('merchant', array('merchant_id' => $merchantId)); 
            if ($deleteStatus) {
                $this->sentResponse("Data", "", "Merchant Deleted", 200, array($token, $merchantId));
            } else {
                $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401, array($token, $merchantId));
            }
        }
        
    }
}