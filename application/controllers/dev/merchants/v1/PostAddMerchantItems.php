<?php
use chriskacerguis\RestServer\RestController;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/RestController.php';
require APPPATH . 'libraries/Format.php';

class postAddMerchantItems extends RestController {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_post() {
        $token = $this->post('token');
        $merchantId = $this->post('merchantId');
        $itemName = $this->post('itemName');
        $itemDesc = $this->post('itemDesc');
        $itemPrice = $this->post('itemPrice');
        $itemAvail = $this->post('itemAvail');
        $itemStock = $this->post('itemStock');
        $itemWeight = $this->post('itemWeight');
        $itemPhotos1 = $this->post('photosUrl1');
        $itemPhotos2 = $this->post('photosUrl2');
        $itemPhotos3 = $this->post('photosUrl3');
        $itemPhotos4 = $this->post('photosUrl4');
        $itemPhotos5 = $this->post('photosUrl5');
        $itemCategory = $this->post('itemCategory');
        $itemCategory2 = $this->post('itemCategory2');
        $itemCategory3 = $this->post('itemCategory3');
        $checker = $this->db->get_where('credential', array('token' => $token))->result();
        if (count($checker) == 1 ){
            $merchantItemAddData = array(
                'merchant_id'  => $merchantId,
                'item_name'     => $itemName,
                'item_desc'    => $itemDesc,
                'item_price'    => $itemPrice,
                'item_availability' => $itemAvail,
                'item_stock'    => $itemStock,
                'item_weight'   => $itemWeight,
                'item_photos1'  => $itemPhotos1,
                'item_photos2'  => $itemPhotos2,
                'item_photos3'  => $itemPhotos3,
                'item_photos4'  => $itemPhotos4,
                'item_photos5'  => $itemPhotos5,
                'item_category' => $itemCategory,
                'item_category2' => $itemCategory2,
                'item_category3' => $itemCategory3,
            );
            $addResult = $this->db->insert('merchant_item', $merchantItemAddData);
            if ($addResult){
                $response = [];
                $this->response($response, 200);
            } else {
                $response['Title'] =  "Oops something wrong, try again later!.";
                $response['Code'] = 10004;
                $response['Message'] = "";
                $this->response($response, 401);
            }
        } else {
            $response['Title'] =  "Oops Credential not valid, please login again!.";
            $response['Code'] = 10001;
            $response['Message'] = "";
            $this->response($response, 402);
        }   
    }
}