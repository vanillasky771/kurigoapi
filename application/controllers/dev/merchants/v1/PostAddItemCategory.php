<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class postAddItemCategory extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_post() {
        $token = $this->post('token');
        $categoryName = $this->post('categoryName');
        $categoryType = $this->post('categoryType');

        $checker = $this->db->get_where('credential', array('token' => $token))->result();
        if (count($checker) == 1 ){   
            $categoryData = array(
                'category_name' => $categoryName,
                'category_type' => $categoryType
            );     
            $insertStatus = $this->db->insert('item_category', $categoryData);
            if ($insertStatus) {
                $this->sentResponse("Data", $categoryData, "Category Added", 200,$categoryData);
            } else {
                $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401, $categoryData);
            }
        } else {
            $this->sendErrorResponse("Oops Credential not valid, please login again!.", 10001, "", 402, $token);
        }
    }
}