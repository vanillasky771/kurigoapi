<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class getMerchantCategory extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_get() {
        $token = $this->get('token');
        $type = $this->get('type');
        $checker = $this->db->get_where('credential', array('token' => $token))->result();
        if (count($checker) == 1 ){
            $this->db->where('category_class', $type);
            $itemCategoryData = $this->db->get('item_category')->result();
            if (!empty($itemCategoryData)) {
                $this->sentResponse("CategoryList", $itemCategoryData, "", 200, array($token, $type));
            } else if (empty($itemCategoryData)) {
                $this->sentResponse("CategoryList", [], "", 200, array($token, $type));
            } else {
                $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401, array($token, $type));
            }
        } else {
            $this->sendErrorResponse("Oops Credential not valid, please login again!.", 10001, "", 402, $token);
        }
    }
}