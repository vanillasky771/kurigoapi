<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class postMenuCategoryList extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_post() {
        $merchantId = $this->post('merchantID');
        $token = $this->post('token');
        $checker = $this->db->get_where('merchant_credential', array('token' => $token))->result();
        if (count($checker) == 1 ){
            $this->db->where('merchant_id', $merchantId);
            $itemCategoryData = $this->db->get('merchant_itemCategory')->result();
            if (!empty($itemCategoryData)) {
                foreach($itemCategoryData as $result){
                    $result->isActive = (bool) $result->isActive;
                }
                $this->sentResponse("CategoryList", $itemCategoryData, "", 200,$merchantId);
            } else if (empty($itemCategoryData)) {
                $this->sentResponse("CategoryList", [], "", 200,$merchantId);
            } else {
                $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401, $merchantId);
            }
        } else {
            $this->sendErrorResponse("Oops Credential not valid, please login again!.", 10001, "", 402, $token);
        }
    }
}