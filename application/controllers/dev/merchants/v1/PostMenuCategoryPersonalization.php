<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class postMenuCategoryPersonalization extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_post() {
        $categoryId = $this->post('menuCategoryId');
        $token = $this->post('token');
        $name = $this->post('categoryName');
        $description = $this->post('categoryDesc');
        $isActive = $this->post('isActive');
        $checker = $this->db->get_where('merchant_credential', array('token' => $token))->result();
        if (count($checker) == 1 ){
            $categoryPesonalization = array(
                'category_name' => $name,
                'category_description' => $description,
                'isActive' => $isActive
            );
            $this->db->where('merchcategory_id', $categoryId);
            $updateResult = $this->db->update('merchant_itemCategory', $categoryPesonalization);
            if ($updateResult){
                $this->sentResponse("Data", $categoryPesonalization, "Menu Updated", 200,$categoryPesonalization);
            } else {
                $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401, $categoryPesonalization);
            }
        } else {
            $this->sendErrorResponse("Oops Credential not valid, please login again!.", 10001, "", 402, $token);
        }
    }
}