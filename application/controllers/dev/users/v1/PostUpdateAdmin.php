<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class postUpdateAdmin extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_post() {
        $token = $this->post('token');
        $adminId = $this->post ('id');
        $newBranch = $this->post('branch');
        $password = $this->post('password');
        $checker = $this->db->get_where('credential', array('token' => $token))->result();
        if (count($checker) == 1 ){
            if (empty($password) && !empty($adminId)) {
                $adminBranchUpdate = array(
                    'branch'        => $newBranch
                );
            } else if (!empty($adminId)) {
                $adminBranchUpdate = array(
                    'branch'        => $newBranch,
                    'password' => md5($password)
                );
            }
            $this->db->where('admin_id', $adminId);
            $updateResult = $this->db->update('admins', $adminBranchUpdate);
            if ($updateResult){
                $response = [];
                $this->response($response, 200);
            } else {
                $response['Title'] =  "Oops something wrong, try again later!.";
                $response['Code'] = 10004;
                $response['Message'] = $this->db->error();
                $this->response($response, 401);
            }
        } else {
            $response['Title'] =  "Oops Credential not valid, please login again!.";
            $response['Code'] = 10001;
            $response['Message'] = "";
            $this->response($response, 402);
        }
    }
}
//NTM