<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class GetReceiverList extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
        $CI = &get_instance();
        $this->db2 = $CI->load->database('logging', TRUE);
    }

    function index_get() {
        $customerId = $this->get('custId');
        $token = $this->get('token');
        $checker = $this->db->get_where('credential', array('token' => $token))->result();
        if (count($checker) == 1 ){
            $receiverList = $this->db->get_where('customer_receiver', array('customer_id' => $customerId))->result();
            if (!empty($receiverList)) {
                $response['receiverList'] = $receiverList;
                $code = 200;
            } else if (empty($orderList)) {
                $response['receiverList'] = [];
                $code = 200;
            } else {
                $response['Title'] =  "Oops something wrong, try again later!.";
                $response['Code'] = 10004;
                $response['Message'] = "";
                $code = 401;
            }
        } else {
            $response['Title'] =  "Oops Credential not valid, please login again!.";
            $response['Code'] = 10001;
            $response['Message'] = "";
            $code = 402;
        }
        $respArray = array ('status' => $code, 
        'response' => $response);
        $param = array('param' => "");
        $logger = array(
            'url' => current_url(),
            'param' => json_encode($param),
            'response' => json_encode($respArray)
        );
        $this->db2->insert('apiLog', $logger);
        $this->response($response, $code);
    }   
}