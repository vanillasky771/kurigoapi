<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class putDeleteAdmin extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_put() {
        $admin_id = $this->put('id');
        $deleteStatus = $this->db->delete('admins', array('admin_id' => $admin_id)); 
        if ($deleteStatus) {
            $responsse['Message'] = "Deleted";
            $this->response($responsse, 200);
        } else {
            $response['Title'] =  "Oops something wrong, try again later!.";
            $response['Code'] = 10004;
            $response['Message'] = "";
            $this->response($response, 401);
        }
    }
}
//NTM