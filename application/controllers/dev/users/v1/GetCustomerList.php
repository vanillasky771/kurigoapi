<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class getCustomerList extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_get() {
        $token = $this->get('token');
        $checker = $this->db->get_where('credential', array('token' => $token))->result();
        if (count($checker) == 1 ){
            $customerList = $this->db->get('customer')->result();
            if (!empty($customerList)) {
                $response['CustomerList'] = $customerList;
                $this->response($response);
            } else if (empty($orderList)) {
                $response['CustomeerList'] = [];
                $this->response($response, 200);
            } else {
                $response['Title'] =  "Oops something wrong, try again later!.";
                $response['Code'] = 10004;
                $response['Message'] = "";
                $this->response($response, 401);
            }
        } else {
            $response['Title'] =  "Oops Credential not valid, please login again!.";
            $response['Code'] = 10001;
            $response['Message'] = "";
            $this->response($response, 402);
        }
    }
}