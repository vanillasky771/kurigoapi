<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class postDriverPendapatan extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_post() {
        $uniq = $this->post('uniq');
        $dateFilter = $this->post('date');
        $checker = $this->db->get_where('driver_credential', array('uniqueUser' => $uniq))->result();
        if (count($checker) == 1 ){
            $driverId = array_column($checker, "drivercredential_id")[0];
            $this->db->select('reference_code,order_created,order_date,order_rate, order_fee,driver_order_point', FALSE);//
            $array = array('assigned_driver' => $driverId, 'order_date' => $dateFilter, 'order_status' => "Delivered");
            $this->db->where($array);
            $query = $this->db->get('order_transaction');
            $driverData = $query->result();
            if (!empty($driverData)) {
                $response['TotalTrips'] = count($driverData);
                $response['Activities'] = $driverData;
                $this->sentResponse("Data", $response, "", 200,array($uniq, $dateFilter));
            } else {
                $response['TotalTrips'] = 0;
                $response['Activities'] = [];
                $this->sentResponse("Data", $response, "", 200,array($uniq, $dateFilter));
            }
        } else {
            $this->sendErrorResponse("Oops Credential not valid, please login again!.", 10001, "", 402,array($uniq, $dateFilter));
        }
    }
}