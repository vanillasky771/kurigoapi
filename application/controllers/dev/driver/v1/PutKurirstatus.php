<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class putKurirstatus extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_put() {
        $token = $this->put('token');
        $status = $this->put('status');
        $driverId = $this->put('driverId');

        $checker = $this->db->get_where('credential', array('token' => $token))->result();
        if (count($checker) == 1 ){        
            $kurirUpdateData = array(
                'driver_status' => $status,
            );
            $this->db->where('driver_id', $driverId);
            $updateStatus = $this->db->update('driver', $kurirUpdateData);
            if ($updateStatus) {
                $this->sentResponse("Data", "", "Driver Updated", 200,$kurirUpdateData);
            } else {
                $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401, array($token, $status, $driverId));
            }
        } else {
            $this->sendErrorResponse("Oops Credential not valid, please login again!.", 10001, "", 402,array($token, $status, $driverId));
        }
    }
}