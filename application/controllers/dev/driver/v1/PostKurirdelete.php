<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class postKurirDelete extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_post() {
        $token = $this->post('token');
        $driver_credId = $this->post('id');
        $checker = $this->db->get_where('credential', array('token' => $token))->result();
        if (count($checker) == 1 ){
            $deleteStatus = $this->db->delete('driver_credential', array('drivercredential_id' => $driver_credId)); 
            if ($deleteStatus) {
                $this->sentResponse("Data", "", "Driver Deleted", 200,array($token, $driver_credId));
            } else {
                $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401, array($token, $driver_credId));
            }
        } else {
            $this->sendErrorResponse("Oops Credential not valid, please login again!.", 10001, "", 402,array($token, $driver_credId));
        }
    }
}