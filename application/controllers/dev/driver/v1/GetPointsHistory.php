<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class getPointsHistory extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_get() {
        $uniq = $this->get('driverId');
        $this->db->select('sum(points_gained) as total');
        $totalsPoint = $this->db->get_where('points',array('driver_uniqID' => $uniq))->result();

        $this->db->select('points.*, order_transaction.reference_code as order_ref');
        $this->db->from('points');
        $this->db->join('order_transaction', 'order_transaction.order_id=points.order_id');
        $this->db->where('points.driver_uniqID', $uniq);
        $pointsData = $this->db->get()->result();
        if (!empty($pointsData)) {
            $response['TotalPoints'] = $totalsPoint[0]->total;
            $response['PointLists'] = $pointsData;
            $this->sentResponse("Data", $response, "", 200,$uniq);
        } else {
            $response['TotalPoints'] = 0;
            $response['PointLists'] = [];
            $this->sentResponse("Data", $response, "", 200,$uniq);
        }
    }
}