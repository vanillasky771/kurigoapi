<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class postDriverLocation extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_post() {
        $id = $this->post('driverID');
        $currentLat = $this->post('lat');
        $currentLong = $this->post('long');
        $kurirUpdateData = array(
            'driver_currentLat' => $currentLat,
            'driver_currentLong' => $currentLong
        );
        $this->db->where('driver_id', $id);
        $updateStatus = $this->db->update('driver', $kurirUpdateData);
        if ($updateStatus) {
            $response['Data'] = "";
            $response['Message'] = "Updated";
            $response['Status'] = 200;
            $this->response($response);
        } else {
            $response['Title'] =  "Oops something wrong, try again later!.";
            $response['Code'] = 10004;
            $response['Message'] = "";
            $response['Status'] = 401;
            $this->response($response, 401);
        }
    }
}