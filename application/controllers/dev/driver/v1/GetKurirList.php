<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class getKurirList extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_get() {
        $token = $this->get('token');
        $checker = $this->db->get_where('credential', array('token' => $token))->result();
        if (count($checker) == 1 ){
            $driverData = $this->db->get('driver')->result();
            if (!empty($driverData)) {
                $this->sentResponse("KurirList", $driverData, "", 200,$token);
            } else {
                $this->sentResponse("KurirList", [], "", 200,$token);
            }
        } else {
            $this->sendErrorResponse("Oops Credential not valid, please login again!.", 10001, "", 402,$token);
        }
    }
}