<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class postKurirUpdate extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_post() {
        $token = $this->post('token');
        $id = $this->post('id');
        $name = $this->post('name');
        $address = $this->post('address');
        $hp = $this->post('hp');
        $codLimit = $this->post('cod_limit');
        $status = $this->post('status');
        $pp = $this->post('profilepicture');
        $vehicleType = $this->post("vehicleType");
        $platNumber = $this->post("platNumber");
        $simCNumber = $this->post("simCNumber");
        $stnkNumber = $this->post("stnkNumber");
        $simImagePath = $this->post("simImagePath");
        $stnkImagePath = $this->post("stnkImagePath");
        $checker = $this->db->get_where('credential', array('token' => $token))->result();
        if (count($checker) == 1 ){
            $kurirUpdateData = array(
                'driver_name' => $name,
                'driver_address' => $address,
                'driver_hp' => $hp,
                'cod_limit' => $codLimit,
                'driver_status' => $status,
                'driver_vehicle' => $vehicleType,
                'driver_plateNumber' => $platNumber,
                'driver_simCNumber' => $simCNumber,
                'driver_stnkNumber' => $stnkNumber,
                'driver_simimagepath' => $simImagePath,
                'driver_stnkimagePath' => $stnkImagePath,
                'driver_profilepicture' => $pp
            );
            $this->db->where('driver_id', $id);
            $updateStatus = $this->db->update('driver', $kurirUpdateData);
            if ($updateStatus) {
                $this->sentResponse("Data", "", "Driver Updated", 200,$kurirUpdateData);
            } else {
                $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401, array($token, $id));
            }
        } else {
            $this->sendErrorResponse("Oops Credential not valid, please login again!.", 10001, "", 402,array($token, $id));
        }
    }
}