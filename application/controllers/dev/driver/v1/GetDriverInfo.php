<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class getDriverInfo extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_get() {
        $uniq = $this->get('uniq');
        $checker = $this->db->get_where('driver_credential', array('uniqueUser' => $uniq))->result();
        if (count($checker) == 1 ){
            $driverPhone = array_column($checker, "phone");
            $driverData = $this->db->get_where('driver', array('driver_hp' => $driverPhone[0]))->result();
            if (!empty($driverData)) {
                $response['driverID'] = array_column($driverData, "driver_id")[0];
                $response['TotalTrips'] = array_column($driverData, "totalTrips")[0];
                $response['TodayGet'] = array_column($driverData, "todayTripGet")[0];
                $response['DriverName'] = array_column($driverData, "driver_name")[0];
                $this->sentResponse("Data", $response, "", 200,$uniq);
            } else {
                $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401, $uniq);
            }
        } else {
            $this->sendErrorResponse("Oops Credential not valid, please login again!.", 10001, "", 402,$uniq);
        }
    }
}