<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class getDriverProfile extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
        $CI = &get_instance();
        $this->db2 = $CI->load->database('logging', TRUE);
    }

    function index_get() {
        $uniq = $this->get('uniq');
        $checker = $this->db->get_where('driver_credential', array('uniqueUser' => $uniq))->result();
        if (count($checker) == 1 ){
            $driverPhone = array_column($checker, "phone")[0];
            $driverData = $this->db->get_where('driver', array('driver_hp' => $driverPhone))->result();
            if (!empty($driverData)) {
                $response['Name'] = array_column($driverData, "driver_name")[0];
                $response['Phone'] = $driverPhone;
                $response['PlateNumber'] = array_column($driverData, "driver_plateNumber")[0];
                $response['Points'] = array_column($driverData, "driver_points")[0];
                $response['Ranks'] = array_column($driverData, "driver_rank")[0];
                $response['Rate'] = array_column($driverData, "driver_rate")[0];
                $this->sentResponse("Data", $response, "", 200,$uniq);
            } else {
                $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401, $uniq);
            }
        } else {
            $this->sendErrorResponse("Oops Credential not valid, please login again!.", 10001, "", 402,$uniq);
        }
    }
}