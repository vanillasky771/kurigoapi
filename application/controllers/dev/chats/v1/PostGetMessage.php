<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class postGetMessage extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_post() {
        $sender = $this->post('sender');
        $receiver = $this->post('receiver');

        $chatsData = $this->db->get_where('chats',array('chat_sender' => $sender, 'chat_receiver' => $receiver))->result();
        if (!empty($chatsData)) {
            $this->sentResponse("Messages", $chatsData, "", 200,array("sen" => $sender, "rec" => $receiver));
        } else {
            $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401,array("sen" => $sender, "rec" => $receiver));
        }
    }
}