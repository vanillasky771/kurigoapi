<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class getChatsList extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_get() {
        $uniq = $this->get('uniqID');
        $this->db->select('chat_id,chat_sender,chat_receiver,chat_created, customer.customer_name as receiver_name');
        $this->db->from('chats');
        $this->db->join('customer', 'chats.chat_receiver=customer.customer_uniqueAddress');
        $this->db->where('chats.chat_sender =', $uniq);
        $this->db->order_by('chats.chat_created','DESC');
        $this->db->group_by('chats.chat_receiver');
        $chatListData = $this->db->get()->result();
        if (!empty($chatListData)) {
            $this->sentResponse("ChatList", $chatListData, "", 200,$uniq);
        } else {
            $this->sentResponse("ChatList", [], "", 200,$uniq);
        }
    }
}