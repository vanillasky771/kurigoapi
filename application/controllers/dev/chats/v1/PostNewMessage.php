<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class postNewMessage extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_post() {
        $sender = $this->post('sender');
        $receiver = $this->post('receiver');
        $body = $this->post('message');

        $dataMessage = array(
            'chat_sender' => $sender,
            'chat_receiver' => $receiver,
            'chat_body' => $body
        );
        $this->db->insert('chats', $dataMessage);
        $this->sentResponse("Data", $body, "", 200,array("sen" => $sender, "rec" => $receiver));
    }
}