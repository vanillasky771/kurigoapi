<?php
use chriskacerguis\RestServer\RestController;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/RestController.php';
require APPPATH . 'libraries/Format.php';

class postMerchOrderVer extends RestController {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_post() {
        $userType = $this->get('type');
        $promotionData = $this->db->get_where('promotion',array('promotion_type' => $userType))->result();
        if (!empty($versionsData)) {
            $responsse['Promotions'] = $promotionData;
            $this->response($responsse);
        } else {
            $response['Title'] =  "Oops something wrong, try again later!.";
            $response['Code'] = 10004;
            $response['Message'] = "";
            $this->response($response, 401);
        }
    }
}