<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class postNewOrder extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_post() {
        $refCode = "KRG-".date("ymdhm")."-".rand(1111,9999);
        $customerId = $this->post('customerId');
        $pickupAddress = $this->post('pickup_address');
        $pickupLat = $this->post('pickupLat');
        $pickupLong = $this->post('pickupLong');
        $orderPickupTime = $this->post('pickup_time');
        $receiverId = $this->post('receiver_id');
        $sentAddress = $this->post('sent_address');
        $sentLat = $this->post('sentLat');
        $sentLong = $this->post('sentLong');
        $timeRemaining = $this->post('timeRemaining');
        $totalprice = $this->post('totalPrice');
        $rate = $this->post('driverRate');
        $amenities = $this->post('amenities');
        $items = $this->post('items');
        $price = $this->post('orderPrice');
        $paymentmethod = $this->post('paymentmethod');
        $packageWidth = $this->post('dimensional_width');
        $packageHeight = $this->post('dimensional_height');
        $assigned_driver = $this->post('assigned_driver');
        $useVoucher = $this->post('isVoucherused');
        $voucherid  = $this->post('voucher');
        $city = $this->post('city');
        $vehicletype = $this->post('vehicle_type');
        $servicetype = $this->post('service_type');
        $isFood = $this->post('isFood');
        $merchantId = $this->post('merchantId');
        $merctotalprice = $this->post('merchantTotalPrice');
        $orderfee = $this->post('fee');
        if ($isFood == "true") {
            $pickuppasscode = rand(1111,9999);
        } else {
            $pickuppasscode = 0;
        }
        
        $driverorderpoint = ""; //generate logic belom ada

        $orderData = array(
            'reference_code' => $refCode,
            'customer_id' => $customerId,
            'pickup_location' => $pickupAddress,
            'pickup_lat' => $pickupLat,
            'pickup_long' => $pickupLong,
            'order_pickuptime' => $orderPickupTime,
            'receiver_id' => $receiverId,
            'sent_address' => $sentAddress,
            'sent_lat' => $sentLat,
            'sent_long' => $sentLong,
            'order_status' => "Waiting",
            'time_remaining' => "0".$timeRemaining.":00:00",
            'order_date' => date("Y-m-d h:m:s"),
            'order_totalPrice' => $totalprice,
            'order_rate' => $rate,
            'order_amenities' => $amenities,
            'order_items' => $items,
            'order_price' => $price,
            'order_paymentmethod' => $paymentmethod,
            'order_width' => $packageWidth,
            'order_height' => $packageHeight,
            'assigned_driver' => $assigned_driver,
            'isUsingVoucher' => $useVoucher,
            'voucher_id' => $voucherid,
            'order_city' => $city,
            'vehicle_type' => $vehicletype,
            'service_type' => $servicetype,
            'isFood' => $isFood,
            'merchant_id' => $merchantId,
            'merchant_price' => $merctotalprice,
            'order_fee' => $orderfee,
            'pickup_passcode' => $pickuppasscode,
            'driver_order_point' => $driverorderpoint
        );
        $insertStatus = $this->db->insert('order_transaction', $orderData);
        $this->sentResponse("OrderData", $orderData, "Success", 200,"");
    }
}