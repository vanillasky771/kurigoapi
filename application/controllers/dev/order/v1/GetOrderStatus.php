<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class getOrderStatus extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_get() {
        $refcode = $this->get('refCode');
        $checker = $this->db->get_where('order_transaction', array('reference_code' => $refcode))->result();
        if (!empty($checker)) {
            $response['isReadyToPickup'] = array_column($checker,"isReadytoPickup")[0];
            $response['isPickedUp'] = array_column($checker,"isPickedUp")[0];
            $this->sentResponse("Data", $response, "", 200,$refcode);
        } else {
            $this->sendErrorResponse("No Order with this reference code", 10006, "", 401, array('orderId'=> $id));
        }
    }
}