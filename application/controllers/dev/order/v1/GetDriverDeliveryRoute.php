<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class getDriverDeliveryRoute extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
        $CI = &get_instance();
        $this->db2 = $CI->load->database('logging', TRUE);
    }

    function index_get() {
        $driverId = $this->get('driverId');
        $orderId = $this->get('orderId');
        $token = $this->get('token');
        
        $driverChecker = $this->db->get_where('driver_credential', array('token' => $token))->result();
        if (count($driverChecker) == 1){
            $this->db->select('driver_currentLat, driver_currentLong');
            $driverData = $this->db->get_where('driver', array('driver_id' => $driverId))->result();
            
            $this->db->select('sent_lat, sent_long, isPickedUp, pickup_lat, pickup_long, merchant_id, isFood');
            $orderData = $this->db->get_where('order_transaction', array('order_id' => $orderId))->result();
            
            if ($driverData[0]->driver_currentLat != "" && $driverData[0]->driver_currentLong != "") {
                $driverLoc = urlencode($driverData[0]->driver_currentLat.",".$driverData[0]->driver_currentLong);
            } else {
                $driverLoc = urlencode("0,0");
            }
            if (count($orderData) != 0) {
                if ($orderData[0]->isFood == 1) {
                    $this->db->select('merchant_id, merchant_address, merchant_lat, merchant_long');
                    $merchantData = $this->db->get_where('merchant', array('merchant_id' => $orderData[0]->merchant_id))->result();

                    if ($orderData[0]->isPickedUp == 0) {
                        if ($orderData[0]->pickup_lat != "" && $orderData[0]->pickup_long != "") {
                            $deliveryLoc = urlencode($merchantData[0]->merchant_lat.",".$merchantData[0]->merchant_long);
                        } else {
                            $deliveryLoc = urlencode("0,0");
                        }
                    } else {
                        if ($orderData[0]->sent_lat != "" && $orderData[0]->sent_long != "") {
                            $deliveryLoc = urlencode($orderData[0]->sent_lat.",".$orderData[0]->sent_long);
                        } else {
                            $deliveryLoc = urlencode("0,0");
                        }
                    }
                } else {
                    if ($orderData[0]->isPickedUp == 0) {
                        if ($orderData[0]->pickup_lat != "" && $orderData[0]->pickup_long != "") {
                            $deliveryLoc = urlencode($orderData[0]->pickup_lat.",".$orderData[0]->pickup_long);
                        } else {
                            $deliveryLoc = urlencode("0,0");
                        }
                    } else {
                        if ($orderData[0]->sent_lat != "" && $orderData[0]->sent_long != "") {
                            $deliveryLoc = urlencode($orderData[0]->sent_lat.",".$orderData[0]->sent_long);
                        } else {
                            $deliveryLoc = urlencode("0,0");
                        }
                    }
                }
                
                $url = "https://maps.googleapis.com/maps/api/directions/json?origin=".$driverLoc."&destination=".$deliveryLoc."&key=AIzaSyB4bS-jn1gypbZma_lRTHPBgTBB989geMI&avoid=highways";
                $response = file_get_contents($url);
                $decodedResponse = json_decode($response, true);
                if ($decodedResponse["status"] == "OK") {
                    $routes = $decodedResponse["routes"][0]["legs"];
                    $this->sentResponse("results", $routes, "", 200,array($driverLoc, $deliveryLoc, $driverId, $orderId));
                } else {
                    $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401, array($driverLoc, $deliveryLoc, $driverId, $orderId));
                }
            } else {
                $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401, array($driverLoc, $deliveryLoc, $driverId, $orderId));
            }
        } else {
            $this->sendErrorResponse("Oops Credential not valid, please login again!.", 10001, "", 402,array($driverId, $orderId));
        }
    }
}