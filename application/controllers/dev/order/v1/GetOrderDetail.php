<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH . 'controllers/ResponseSender.php';
class getOrderDetail extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database(); 
    }

    function index_get() {
        $id = $this->get('id');
        $this->db->select('order_transaction.*, customer.customer_name, customer.customer_lat, customer.customer_phone, customer.customer_long, customer.customer_address, customer_receiver.receiver_name, customer_receiver.receiver_phone, merchant.merchant_displayName as merchantName');
        $this->db->from('order_transaction');
        $this->db->join('customer', 'order_transaction.customer_id=customer.customer_id');
        $this->db->join('merchant', 'order_transaction.merchant_id=merchant.merchant_id');
        $this->db->join('customer_receiver', 'order_transaction.receiver_id=customer_receiver.receiver_id');
        $this->db->where('order_id', $id);
        $checker = $this->db->get()->result();
        foreach($checker as $row) {
            $weightTotal = 0;
            if ($row->assigned_driver != NULL || $row->assigned_driver != 0) {
                $this->db->select('driver_name, driver_hp,');
                $this->db->from('driver');
                $this->db->where('driver_id', $row->assigned_driver);
                $driverInfo = $this->db->get()->result();
                if (count($driverInfo) != 0) {
                    $row->driver_name = $driverInfo[0]->driver_name;
                    $row->driver_hp = $driverInfo[0]->driver_hp;
                } else {
                    $row->driver_name = "";
                    $row->driver_hp = "";
                }
            } else {
                $row->driver_name = "";
                $row->driver_hp = "";
            }
            $amenitiesData = $this->db->get_where('order_amenities', array('order_reference' => $row->reference_code))->result();
            if (count($amenitiesData) != 0) {
                $row->order_amenities = $amenitiesData;
            }
            
            $this->db->select("sent_address, sent_lat, sent_long, order_weight");
            $receiver_data = $this->db->get_where('order_transaction', array('reference_code' => $row->reference_code))->result();
            if(count($receiver_data) != 0) {
               $row->receiverList = $receiver_data;
               foreach($receiver_data as $rows) {
                   $weightTotal = $weightTotal + $rows->order_weight;
               }
               $row->totalWeight = $weightTotal;
            }
        }
        if (!empty($checker)) {
            $this->sentResponse("OrderDetail", $checker[0], "", 200, array('orderId'=> $id));
        } else {
            $this->sendErrorResponse("No Order with this id", 10005, "", 401, array('orderId'=> $id));
        }
    }
}