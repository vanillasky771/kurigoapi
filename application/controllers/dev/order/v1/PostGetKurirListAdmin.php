<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class postGetKurirListAdmin extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
        $CI = &get_instance();
        $this->db2 = $CI->load->database('logging', TRUE);
    }

    function index_post() {
        $pickupLat = $this->post('pickupLat');
        $pickupLong = $this->post('pickupLong');
        $token = $this->post('token');

        $checker = $this->db->get_where('credential', array('token' => $token))->result();
        if (count($checker) == 1 ){
            $this->db->select('driver_currentLat, driver_currentLong, driver_id, driver_name, driver_hp');
            $this->db->where('driver_status != "On-Trip"');
            $this->db->where('driver_status != "Offline"');
            $driverData = $this->db->get('driver')->result();
            if (count($driverData) != 0) {
                foreach($driverData as $row) {
                    $driverLoc = urlencode($row->driver_currentLat.",".$row->driver_currentLong); 
                    $pickupLoc = urlencode($pickupLat.",".$pickupLong);
                    $url = "https://maps.googleapis.com/maps/api/directions/json?origin=".$driverLoc."&destination=".$pickupLoc."&key=AIzaSyB4bS-jn1gypbZma_lRTHPBgTBB989geMI&avoid=highways";
                    $response = file_get_contents($url);
                    $decodedResponse = json_decode($response, true);
                    if ($decodedResponse["status"] == "OK") {
                        $routes = $decodedResponse["routes"];
                        $distance = $routes[0]["legs"][0]["distance"]["text"];
                        $distances = $res = preg_replace("/[^0-9.]/", "", $distance);
                        $row->distance = $distance;
                        $row->intDistance = $distances;
                    } else {
                        $row->intDistance = 0;
                    }
                }
                usort($driverData, function($a, $b) { return $a->intDistance - $b->intDistance; });
                $this->sentResponse("KurirList", $driverData, "", 200,array($token, $pickupLat, $pickupLong));
            } else {
                $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401, array($token, $pickupLat, $pickupLong));
            }
        } else {
            $this->sendErrorResponse("Oops Credential not valid, please login again!.", 10001, "", 402,array($token, $pickupLat, $pickupLong));
        }
    }
}