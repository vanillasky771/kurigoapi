<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class postOrderFinish extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_post() {
        $refcode = $this->post('orderId');
        if (!empty($refcode)) {
            $this->db->select('order_id, assigned_driver,driver_order_point as driverPoint');
            $orderData = $this->db->get_where('order_transaction', array('order_id' => $refcode))->result();
            
            $addDriverPoint = array(
                'driver_uniqID' => $orderData[0]->assigned_driver,
                'order_id' => $orderData[0]->order_id,
                'points_gained' => $orderData[0]->driverPoint
            );
            $insertPointStatus = $this->db->insert('points', $addDriverPoint);
            $updateStatus = array(
                'order_status' => "Delivered",
            );
            $this->db->where('order_id', $refcode);
            $updateStatus = $this->db->update('order_transaction', $updateStatus);
            $updateOrderStatus = array (
                'delivered_time' => date("H:i:s")
            );
            $this->db->where('order_id', $refcode);
            $updateStatus = $this->db->update('order_status', $updateOrderStatus);
            //UPDATE DRIVER POINTS
            $point = 0;
            $pointData = $this->db->get_where('points', array('driver_uniqID' => $orderData[0]->assigned_driver))->result();
            
            foreach ($pointData as $row) {
                $point = $point + $row->points_gained;
            }
            //UPDATE TOTAL TRIPS
            $tripsData = $this->db->get_where('order_transaction', array('assigned_driver' => $orderData[0]->assigned_driver))->result();
            //TODAY's TRIPs
            $todaysTrips = $this->db->get_where('order_transaction', array('assigned_driver' => $orderData[0]->assigned_driver, "order_date" => date("Y-m-d")))->result();
            $updateDriver = array(
                'driver_status' => "Available",
                'totalTrips' => count($tripsData),
                'driver_points' => $point,
                'todayTripGet' => count($todaysTrips)
            );
            $this->db->where('driver_id', $orderData[0]->assigned_driver);
            $driverUpdate = $this->db->update('driver', $updateDriver);
            if ($insertPointStatus && $updateStatus) {
                $this->sentResponse("Data", "", "Order Delivered", 200,$refcode);
            } else {
                $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401, array($refcode));
            }
        } else {
            $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401, array($refcode));
        }
    }
}