<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class postAssignKurir extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_post() {
        $token = $this->post('token');
        $refId = $this->post('orderId');
        $driverId = $this->post('driverId');
        $checker = $this->db->get_where('credential', array('token' => $token))->result();
        if (count($checker) == 1 ){
            $orderChecker = $this->db->get_where('order_transaction', array('order_id' => $refId))->result();
            if (count($orderChecker) == 1) {
                $assignedDriver = array(
                    'assigned_driver'        => $driverId,
                );
                $this->db->where('order_id', $refId);
                $updateResult = $this->db->update('order_transaction', $assignedDriver);
                if ($updateResult){
                    $this->sentResponse("Data", "", "Assigned", 200, array($token, $refId, $driverId));
                } else {
                    $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401, array($token, $refId, $driverId));
                }
            } else {
                $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401, array($token, $refId, $driverId));
            }
        } else {
            $this->sendErrorResponse("Oops Credential not valid, please login again!.", 10001, "", 402,array($token, $refId, $driverId));
        }
    }
}