<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class postOrderList extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_post() {
        $token = $this->post('token');
        $status = $this->post('status');
        $startDate = $this->post('startDate');
        $endDate = $this->post('endDate');
        $filterBy = $this->post('filterBy');
        $driverLat = $this->post('driverLat');
        $driverLong = $this->post('driverLong');
        $param = array('token'=> $token, 'status' => $status, 'startdt' => $startDate, 'enddt' => $endDate,
    'filter' => $filterBy, 'driverLat' => $driverLat, 'driverLong' => $driverLong);
        $checker = $this->db->get_where('credential', array('token' => $token))->result();
        $driverChecker = $this->db->get_where('driver_credential', array('token' => $token))->result();
        if (count($checker) == 1 || count($driverChecker) == 1){
            $this->db->select('order_transaction.*, customer.customer_name, customer.customer_lat, customer.customer_phone, customer.customer_long, customer.customer_address, customer_receiver.receiver_name, customer_receiver.receiver_phone, merchant.merchant_displayName as merchantName');
            $this->db->from('order_transaction');
            $this->db->join('customer', 'order_transaction.customer_id=customer.customer_id', 'inner');
            $this->db->join('merchant', 'order_transaction.merchant_id=merchant.merchant_id', 'left outer');
            $this->db->join('customer_receiver', 'order_transaction.receiver_id=customer_receiver.receiver_id', 'left outer');
            if ($endDate != "" && $startDate != "" && $status == "ALL") {
                $this->db->where('order_transaction.order_date >=', $startDate);
                $this->db->where('order_transaction.order_date <=', $endDate);
                if ($filterBy == "area") {
                    $this->db->order_by('order_city');
                }
                if (count($driverChecker) == 1) {
                    $this->db->group_by('reference_code');
                }
                $query = $this->db->get();
            } else if ($endDate != "" && $startDate != "" && $status != "") {
                $this->db->where('order_transaction.order_date >=', $startDate);
                $this->db->where('order_transaction.order_date <=', $endDate);
                $this->db->where('order_transaction.order_status', $status);
                if ($filterBy == "area") {
                    $this->db->order_by('order_city');
                }
                if (count($driverChecker) == 1) {
                    $this->db->group_by('reference_code');
                }
                $query = $this->db->get();
            } else {
                if ($status != "ALL") {
                    $this->db->where('order_transaction.order_status', $status);
                    if ($filterBy == "area") {
                        $this->db->order_by('order_city');
                    }
                    if (count($driverChecker) == 1) {
                        $this->db->group_by('reference_code');
                    }
                    $query = $this->db->get();
                }
            }
            
            $orderList = $query->result();
            foreach($orderList as $row) {
                $weightTotal = 0;
                if ($row->assigned_driver != NULL || $row->assigned_driver != 0) {
                    $this->db->select('driver_name, driver_hp, driver_currentLat, driver_currentLong');
                    $this->db->from('driver');
                    $this->db->where('driver_id', $row->assigned_driver);
                    $driverInfo = $this->db->get()->result();
                    if (count($driverInfo) != 0) {
                        $row->driver_name = $driverInfo[0]->driver_name;
                        $row->driver_hp = $driverInfo[0]->driver_hp;
                        $driverLat = $driverInfo[0]->driver_currentLat;
                        $driverLong = $driverInfo[0]->driver_currentLong;
                    } else {
                        $row->driver_name = "";
                        $row->driver_hp = "";
                    }
                } else {
                    $row->driver_name = "";
                    $row->driver_hp = "";
                }
                $amenitiesData = $this->db->get_where('order_amenities', array('order_reference' => $row->reference_code))->result();
                if (count($amenitiesData) != 0) {
                    $row->order_amenities = $amenitiesData;
                }
                
                $this->db->select("sent_address, sent_lat, sent_long, order_weight");
                $receiver_data = $this->db->get_where('order_transaction', array('reference_code' => $row->reference_code))->result();
                if(count($receiver_data) != 0) {
                   $row->receiverList = $receiver_data;
                   foreach($receiver_data as $rows) {
                       $weightTotal = $weightTotal + $rows->order_weight;
                   }
                   $row->totalWeight = $weightTotal;
                }
            }

            $driverLoc = urlencode($driverLat.",".$driverLong);
            if ($filterBy == "nearest" || $filterBy == "") {
                foreach($orderList as $row) {
                    $pickupLoc = urlencode($row->pickup_lat.",".$row->pickup_long);
                    $url = "https://maps.googleapis.com/maps/api/directions/json?origin=".$driverLoc."&destination=".$pickupLoc."&key=AIzaSyB4bS-jn1gypbZma_lRTHPBgTBB989geMI&avoid=highways";
                    $response = file_get_contents($url);
                    $decodedResponse = json_decode($response, true);
                    if ($decodedResponse["status"] == "OK") {
                        $routes = $decodedResponse["routes"];
                        $distance = $routes[0]["legs"][0]["distance"]["text"];
                        $distances = $res = preg_replace("/[^0-9.]/", "", $distance);
                        $duration = $routes[0]["legs"][0]["duration"]["text"];
                        $row->distance = $distance;
                        $row->intDistance = $distances;
                        $row->duration = $duration;
                    } else {
                        $row->intDistance = 0;
                    }
                }
                usort($orderList, function($a, $b) { return $a->intDistance - $b->intDistance; });
                if (!empty($orderList)) {
                    $this->sentResponse("OrderList", $orderList, "", 200,$param);
                } else if (empty($orderList)) {
                    $this->sentResponse("OrderList", [], "", 200,$param);
                } else {
                    $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401, $param);
                }
            } else if ($filterBy == "duration") {
                foreach($orderList as $row) {
                    $pickupLoc = urlencode($row->pickup_lat.",".$row->pickup_long);
                    $url = "https://maps.googleapis.com/maps/api/directions/json?origin=".$driverLoc."&destination=".$pickupLoc."&key=AIzaSyB4bS-jn1gypbZma_lRTHPBgTBB989geMI&avoid=highways";
                    $response = file_get_contents($url);
                    $decodedResponse = json_decode($response, true);
                    if ($decodedResponse["status"] == "OK") {
                        $routes = $decodedResponse["routes"];
                        $distance = $routes[0]["legs"][0]["distance"]["text"];
                        $duration = $routes[0]["legs"][0]["duration"]["text"];
                        $durations = $res = preg_replace("/[^0-9.]/", "", $duration);
                        $row->distance = $distance;
                        $row->duration = $duration;
                        $row->intDuration = $durations;
                    } else {
                        $row->intDuration = 0;
                    }
                }
                usort($orderList, function($a, $b) { return $a->intDuration - $b->intDuration; });
                if (!empty($orderList)) {
                    $this->sentResponse("OrderList", $orderList, "", 200,$param);
                } else if (empty($orderList)) {
                    $this->sentResponse("OrderList", [], "", 200,$param);
                } else {
                    $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401, $param);
                }
            } else { 
                if (!empty($orderList)) {
                    $this->sentResponse("OrderList", $orderList, "", 200,$param);
                } else if (empty($orderList)) {
                    $this->sentResponse("OrderList", [], "", 200,$param);
                } else {
                    $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401, $param);
                }
            }
        } else {
            $this->sendErrorResponse("Oops Credential not valid, please login again!.", 10001, "", 402, $param);
        }
    }
}