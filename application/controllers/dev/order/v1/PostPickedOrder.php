<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class PostPickedOrder extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_post() {
        $token = $this->post('token');
        $uniq = $this->post('uniqId');
        $checker = $this->db->get_where('driver_credential', array('token' => $token))->result();
        if (count($checker) == 1 ){
            $driverId = array_column($checker, "drivercredential_id")[0];
            $currentDate = date('Y-m-d');
            $this->db->select('order_transaction.*, customer.customer_name, customer.customer_lat, customer.customer_long, customer.customer_address, merchant.merchant_displayName as merchantName');
            $this->db->from('order_transaction');
            $this->db->join('customer', 'order_transaction.customer_id=customer.customer_id', 'inner');
            $this->db->join('merchant', 'order_transaction.merchant_id=merchant.merchant_id', 'left outer');
            // $this->db->join('driver', 'order_transaction.assigned_driver=driver.driver_id');
            $this->db->where('order_transaction.order_date', $currentDate);
            $this->db->where('order_transaction.order_status = "Ongoing"');
            $this->db->where('order_transaction.assigned_driver', $driverId);
            $this->db->order_by('order_created');
            $pickedOrderList  = $this->db->get()->result();
            
            $this->db->select('order_transaction.*, customer.customer_name, customer.customer_lat, customer.customer_long, customer.customer_address, merchant.merchant_displayName as merchantName');
            $this->db->from('order_transaction');
            $this->db->join('customer', 'order_transaction.customer_id=customer.customer_id', 'inner');
            $this->db->join('merchant', 'order_transaction.merchant_id=merchant.merchant_id', 'left outer');
            // $this->db->join('driver', 'order_transaction.assigned_driver=driver.driver_id');
            $this->db->where('order_transaction.order_date', $currentDate);
            $this->db->where('order_transaction.order_status = "Confirmed"');
            $this->db->or_where('order_transaction.order_status = "PickedUp"');
            $this->db->where('order_transaction.assigned_driver', $driverId);
            $this->db->order_by('order_created');
            $this->db->group_by('reference_code');
            $confirmedOrderList = $this->db->get()->result();
            $orderList = array_merge($confirmedOrderList, $pickedOrderList);
            // var_dump($orderList);
            
            if (!empty($orderList)) {
                foreach($orderList as $row) {
                    $amenitiesData = $this->db->get_where('order_amenities', array('order_reference' => $row->reference_code))->result();
                    if (count($amenitiesData) != 0) {
                        $row->order_amenities = $amenitiesData;
                    }

                    $weightTotal = 0;
                    if ($row->assigned_driver != NULL || $row->assigned_driver != 0) {
                        $this->db->select('driver_name, driver_hp,');
                        $this->db->from('driver');
                        $this->db->where('driver_id', $row->assigned_driver);
                        $driverInfo = $this->db->get()->result();
                        if (count($driverInfo) != 0) {
                            $row->driver_name = $driverInfo[0]->driver_name;
                            $row->driver_hp = $driverInfo[0]->driver_hp;
                        } else {
                            $row->driver_name = "";
                            $row->driver_hp = "";
                        }
                    } else {
                        $row->driver_name = "";
                        $row->driver_hp = "";
                    }
                    $this->db->select('driver_currentLat, driver_currentLong');
                    $driverData = $this->db->get_where('driver', array('driver_id' => $row->assigned_driver))->result();
                    if ($driverData[0]->driver_currentLat != "" && $driverData[0]->driver_currentLong != "") {
                        $driverLoc = urlencode($driverData[0]->driver_currentLat.",".$driverData[0]->driver_currentLong);
                    } else {
                        $driverLoc = urlencode("0,0");
                    }
                    if ($row->isFood == 1) {
                        $this->db->select('merchant_id, merchant_address, merchant_lat, merchant_long');
                        $merchantData = $this->db->get_where('merchant', array('merchant_id' => $row->merchant_id))->result();
    
                        if ($row->isPickedUp == 0) {
                            if ($row->pickup_lat != "" && $row->pickup_long != "") {
                                $deliveryLoc = urlencode($merchantData[0]->merchant_lat.",".$merchantData[0]->merchant_long);
                            } else {
                                $deliveryLoc = urlencode("0,0");
                            }
                        } else {
                            if ($row->sent_lat != "" && $row->sent_long != "") {
                                $deliveryLoc = urlencode($row->sent_lat.",".$row->sent_long);
                            } else {
                                $deliveryLoc = urlencode("0,0");
                            }
                        }
                    } else {
                        if ($row->isPickedUp == 0) {
                            if ($row->pickup_lat != "" && $row->pickup_long != "") {
                                $deliveryLoc = urlencode($row->pickup_lat.",".$row->pickup_long);
                            } else {
                                $deliveryLoc = urlencode("0,0");
                            }
                        } else {
                            if ($row->sent_lat != "" && $row->sent_long != "") {
                                $deliveryLoc = urlencode($row->sent_lat.",".$row->sent_long);
                            } else {
                                $deliveryLoc = urlencode("0,0");
                            }
                        }
                    }
                    $pickupLoc = urlencode($row->pickup_lat.",".$row->pickup_long);
                    $url = "https://maps.googleapis.com/maps/api/directions/json?origin=".$driverLoc."&destination=".$deliveryLoc."&key=AIzaSyB4bS-jn1gypbZma_lRTHPBgTBB989geMI&avoid=highways";
                    $response = file_get_contents($url);
                    $decodedResponse = json_decode($response, true);
                    if ($decodedResponse["status"] == "OK") {
                        $routes = $decodedResponse["routes"];
                        $distance = $routes[0]["legs"][0]["distance"]["text"];
                        $distances = $res = preg_replace("/[^0-9.]/", "", $distance);
                        $duration = $routes[0]["legs"][0]["duration"]["text"];
                        $row->distance = $distance;
                        $row->intDistance = $distances;
                        $row->duration = $duration;
                    } else {
                        $row->intDistance = 0;
                    }

                    $this->db->select("sent_address, sent_lat, sent_long, order_weight");
                    $receiver_data = $this->db->get_where('order_transaction', array('reference_code' => $row->reference_code))->result();
                        if(count($receiver_data) != 0) {
                        $row->receiverList = $receiver_data;
                        foreach($receiver_data as $rows) {
                            $weightTotal = $weightTotal + $rows->order_weight;
                        }
                        $row->totalWeight = $weightTotal;
                    }
                }
                $this->sentResponse("PickedOrderList", $orderList, "", 200, array($token, $uniq));
            } else if (empty($orderList)) {
                $this->sentResponse("PickedOrderList", [], "", 200, array($token, $uniq));
            } else {
                $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401, array($token, $uniq));
            }
        } else {
            $this->sendErrorResponse("Oops Credential not valid, please login again!.", 10001, "", 402, array($token, $uniq));
        }
    }
}