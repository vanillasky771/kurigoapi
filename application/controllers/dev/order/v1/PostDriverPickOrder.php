<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class postDriverPickOrder extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_post() {
        $orderId = $this->post('refCode');
        $driverId = $this->post('driverId');
        if (!empty($orderId) && !empty($driverId)) {
            $updateAssignedDriver = array(
                'assigned_driver' => $driverId,
                'order_status' => "Confirmed"
            );
            $orderChecker = $this->db->get_where('order_transaction', array('reference_code' => $orderId))->result();
            
            if (array_column($orderChecker, "assigned_driver")[0] == 0 || array_column($orderChecker, "assigned_driver")[0] == NULL) {
                $this->db->where('reference_code', $orderId);
                $updateStatus = $this->db->update('order_transaction', $updateAssignedDriver);
                foreach($orderChecker as $row) {
                    $updateOrderStatus = array (
                        'order_id' => $row->order_id,
                        'confirmed_time' => date("H:i:s")
                    );
                    $this->db->insert('order_status', $updateOrderStatus);
                }
                $this->sentResponse("Data", "", "Order updated", 200,array($orderId,$driverId));
            } else {
                $this->sendErrorResponse("Oops something wrong, try again later!.", 10009, "Order already taken by another driver", 409,array($orderId, $driverId));
            }
        } else {
            $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401, array($token, $refId, $driverId));
        }
    }
}