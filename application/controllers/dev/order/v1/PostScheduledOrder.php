<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class postScheduledOrder extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_post() {
        $token = $this->post('token');
        $uniq = $this->post('uniqId');
        $checker = $this->db->get_where('driver_credential', array('uniqueUser' => $uniq))->result();
        if (count($checker) == 1 ){
            $driverId = array_column($checker, "drivercredential_id")[0];
            $currentDate = date('Y-m-d');
            $this->db->select('order_transaction.*, customer.customer_name, customer.customer_lat, customer.customer_long, customer.customer_address, merchant.merchant_displayName as merchantName');
            $this->db->from('order_transaction');
            $this->db->join('customer', 'order_transaction.customer_id=customer.customer_id', 'inner');
            $this->db->join('merchant', 'order_transaction.merchant_id=merchant.merchant_id', 'left outer');
            $this->db->join('driver', 'order_transaction.assigned_driver=driver.driver_id', 'left outer');
            $this->db->where('order_transaction.order_status = "Confirmed"');
            $this->db->where('order_transaction.assigned_driver', $driverId);
            $this->db->where('order_transaction.order_date >', $currentDate);
            $query = $this->db->get();
            $orderList = $query->result();
            if (!empty($orderList)) {
                $this->sentResponse("ScheduledOrderList", $orderList, "", 200, array($token, $uniq));
            } else if (empty($orderList)) {
                $this->sentResponse("ScheduledOrderList", [], "", 200, array($token, $uniq));
            } else {
                $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401, array($token, $uniq));
            }
        } else {
            $this->sendErrorResponse("Oops Credential not valid, please login again!.", 10001, "", 402, array($token, $uniq));
        }
    }
}