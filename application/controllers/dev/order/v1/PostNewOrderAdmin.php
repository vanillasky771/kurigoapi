<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class postNewOrderAdmin extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_post() {
        
        $customerName = $this->post('customerName');
        $customerEmail = $this->post('customerEmail');
        $customerAddress = $this->post('customerAddress');
        $customerPhone = $this->post('customerPhone');
        $customerLat = $this->post('customerLat');
        $customerLong = $this->post('customerLong');
        $receiverList = $this->post('receiverList');
        $pickupAddress = $this->post('pickup_address');
        $orderPickupTime = $this->post('pickup_time');
        $timeRemaining = $this->post('timeRemaining');
        $totalprice = $this->post('totalPrice');
        $rate = $this->post('driverRate');
        $amenities = $this->post('amenities');
        $items = $this->post('items');
        $price = $this->post('orderPrice');
        $paymentmethod = $this->post('paymentmethod');
        $packageDimension = $this->post('dimensionalData');
        $packageWeight = $this->post('dimensionalWeight');
        $assigned_driver = $this->post('assigned_driver');
        $useVoucher = $this->post('isVoucherused');
        $voucherid  = $this->post('voucher');
        $city = $this->post('city');
        $vehicletype = $this->post('vehicle_type');
        $servicetype = $this->post('service_type');
        $isFood = $this->post('isFood');
        $merchantId = $this->post('merchantId');
        $merctotalprice = $this->post('merchantTotalPrice');
        $orderfee = $this->post('fee');
        $this->db->group_by('reference_code');
        $orders = $this->db->get_where('order_transaction', array('order_date' => date("Y-m-d")))->result();
        $numbers = str_pad((count($orders)+1), 5, '0', STR_PAD_LEFT);
        //generate logic belom ada

        $isFood = filter_var($isFood, FILTER_VALIDATE_BOOLEAN);
        if ($isFood) {
            $pickuppasscode = rand(1111,9999);
            $driverorderpoint = "5";
            $refCode = "KRG-".strtoupper($city)."-".date("Ymd")."-".$numbers."F";
        } else {
            $pickuppasscode = 0;
            $driverorderpoint = "3";
            $refCode = "KRG-".strtoupper($city)."-".date("Ymd")."-".$numbers."P";
        }
        $pickupTime = "";
        $orderDate = "";
        list($orderDate, $pickupTime) = explode(',', $orderPickupTime);
        
        
        $url = "https://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($pickupAddress)."+CA&key=AIzaSyB4bS-jn1gypbZma_lRTHPBgTBB989geMI";
        $response = file_get_contents($url);
        $decodedResponse = json_decode($response, true);
        $pickupLat = $decodedResponse["results"][0]["geometry"]["location"]["lat"];
        $pickupLong = $decodedResponse["results"][0]["geometry"]["location"]["lng"];
        $customerCheck = $this->db->get_where('customer', array('customer_phone' => $customerPhone))->result();
        if (count($customerCheck) == 0) {
            $dataRegister = array(
                'CUSTOMER_NAME' => $customerName,
                'CUSTOMER_EMAIL' => $customerEmail,
                'CUSTOMER_ADDRESS' => $customerAddress,
                'CUSTOMER_LAT' => $customerLat,
                'CUSTOMER_LONG' => $customerLong,
                'CUSTOMER_PHONE' => $customerPhone
            );
            $this->db->insert('customer', $dataRegister);
            $customerId = $this->db->insert_id();
        } else {
            $customerId = array_column($customerCheck, "customer_id")[0];
        }
        foreach ($amenities["amenities"] as $key) { 
            $amenitiesData = array(
                'order_reference' => $refCode,
                'amenities_name' => $key["name"],
                'amenities_price' => $key["price"]
            );
            $this->db->insert('order_amenities', $amenitiesData);
        }
        foreach ($receiverList["receiverlist"] as $key) {
            $receiverCheck = $this->db->get_where('customer_receiver', array('customer_id' => $customerId, 'receiver_name' => $key["receiverName"]))->result();
            if (count($receiverCheck) == 0) { 
                $receiverData = array(
                    'customer_id' => $customerId,
                    'receiver_name' => $key["receiverName"],
                    'receiver_address' => $key["receiverAddress"],
                    'receiver_lat' => $key["receiverLat"],
                    'receiver_long' => $key["receiverLong"],
                    'receiver_phone' => $key["receiverPhone"]
                );
                $this->db->insert('customer_receiver', $receiverData);
                $receiverId = $this->db->insert_id();
            } else {
                $receiverId = array_column($receiverCheck, "receiver_id")[0];
            }

            $orderData = array(
                'reference_code' => $refCode,
                'customer_id' => $customerId,
                'pickup_location' => $pickupAddress,
                'pickup_lat' => $pickupLat,
                'pickup_long' => $pickupLong,
                'order_pickuptime' => $pickupTime,
                'receiver_id' => $receiverId,
                'sent_address' => $key["receiverAddress"],
                'sent_lat' => $key["receiverLat"],
                'sent_long' => $key["receiverLong"],
                'order_status' => "Waiting",
                'time_remaining' => "0".$timeRemaining.":00:00",
                'order_date' => $orderDate,
                'order_totalPrice' => $totalprice,
                'order_rate' => $rate,
                'order_amenities' => json_encode($amenities),
                'order_items' => $items,
                'order_price' => $price,
                'order_note' => $key["note"],
                'order_paymentmethod' => $paymentmethod,
                'order_dimensionWidth' => $key["dimension_width"],
                'order_dimensionHeight' => $key["dimension_height"],
                'order_weight' => $key["weight"],
                'assigned_driver' => $assigned_driver,
                'isUsingVoucher' => false,
                'voucher_id' => $voucherid,
                'order_city' => $city,
                'vehicle_type' => $vehicletype,
                'service_type' => $servicetype,
                'isFood' => $isFood,
                'merchant_id' => $merchantId,
                'merchant_price' => $merctotalprice,
                'order_fee' => $orderfee,
                'pickup_passcode' => $pickuppasscode,
                'driver_order_point' => $driverorderpoint
            );
            $insertStatus = $this->db->insert('order_transaction', $orderData);
            // var_dump($this->db->error());
        }
        if ($insertStatus) {
            $this->sentResponse("Data", "", "Added", 200,$orderData);
        } else {
            $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401, $orderData);
        }
    }
}