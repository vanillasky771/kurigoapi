<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class postOrderPickedup extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_post() {
        $refcode = $this->post('orderId');
        if (!empty($refcode)) {
            $orders = $this->db->get_where('order_transaction', array('order_id' => $refcode))->result();
            $updateStatus = array(
                'isPickedUp' => true,
                'order_status' => "Ongoing"
            );

            $orderChecker = $this->db->get_where('order_transaction', array('reference_code' => array_column($orders, "reference_code")[0]))->result();
            foreach($orderChecker as $row) {
                $updateOrderStatus = array (
                    'pickup_time' => date("H:i:s")
                );
                $this->db->where('order_id', $row->order_id);
                $this->db->update('order_status', $updateOrderStatus);
            }

            $this->db->where('reference_code', array_column($orders, "reference_code")[0]);
            $updateStatus = $this->db->update('order_transaction', $updateStatus);
            if ($updateStatus) {
                $this->sentResponse("Data", "", "Order Picked Up", 200, $refcode);
            } else {
                $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401, $refcode);
            }
        } else {
            $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401, $refcode);
        }
    }
}