<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class postOrderPinValidation extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_post() {
        $refcode = $this->post('orderId');
        $pin = $this->post('key');
        $checker = $this->db->get_where('order_transaction',array('order_id' => $refcode))->result();
        if (array_column($checker, "pickup_passcode")[0] == $pin) {
            $updateStatus = array(
                'isPickedUp' => true,
                'order_status' => "Ongoing"
            );
            $this->db->where('order_id', $refcode);
            $updateStatus = $this->db->update('order_transaction', $updateStatus);
            
            $orderChecker = $this->db->get_where('order_transaction', array('reference_code' => array_column($checker, "reference_code")[0]))->result();
            foreach($orderChecker as $row) {
                $updateOrderStatus = array (
                    'pickup_time' => date("H:i:s")
                );
                $this->db->where('order_id', $row->order_id);
                $this->db->update('order_status', $updateOrderStatus);
            }
            if ($updateStatus) {
                $this->sentResponse("Data", "", "PIN Validated", 200, array($refcode, $pin));
            } else {
                $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401, array($refcode));
            }
        } else {
            $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401, array($refcode));
        }
    }
}