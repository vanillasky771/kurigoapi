<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class GetAddressAutoComplete extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_get() {
        $addressKey = $this->get('key');
        
        $url = "https://maps.googleapis.com/maps/api/place/queryautocomplete/json?key=AIzaSyB4bS-jn1gypbZma_lRTHPBgTBB989geMI&input=".urlencode($addressKey);
        $response = file_get_contents($url);
        $decodedResponse = json_decode($response, true);
        $addressList = [];
        foreach ($decodedResponse["predictions"] as $row) {
            array_push($addressList, $row["description"]);
        }
        if (!empty($addressList)) {
            $this->sentResponse("Lists", $addressList, "", 200,$addressKey);
        } else {
            $this->sentResponse("Lists", [], "", 200,$addressKey);
        }
    }
}