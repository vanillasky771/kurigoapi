<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class postOrderImage extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_post() {
        $refcode = $this->post('orderId');
        $image = $this->post('imageBase');        
        if (!empty($refcode)) {
            $orders = $this->db->get_where('order_transaction', array('order_id' => $refcode))->result();
            $name = array_column($orders, "reference_code")[0];
            // define('UPLOAD_DIR', "/home/u1426581/public_html/orderImage/"); //server
            define('UPLOAD_DIR', "orderImage/"); //local
            $img = str_replace(' ', '+', $image);
            $data = base64_decode($img);
            $file = UPLOAD_DIR . $name . '.png';
            $success=file_put_contents($file, $data);
            if ($success) {
                $updateImage = array(
                    'order_image' => "orderImage/".$name.".png",
                );
                $this->db->where('order_id', $refcode);
                $updateStatus = $this->db->update('order_transaction', $updateImage);
                $this->sentResponse("Data", "", "Image Uploaded", 200,$refcode);
            } else {
                $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401, array($refcode));
            }
        } else {
            $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401, array($refcode));
        }
    }
}