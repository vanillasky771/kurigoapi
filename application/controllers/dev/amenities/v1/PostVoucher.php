<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH . 'controllers/ResponseSender.php';

class postVoucher extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_post() {
        $voucher_name = $this->post('name');
        $voucher_startDate = $this->post('startDate');
        $voucher_endDate = $this->post('endDate');
        $voucher_value = $this->post('value');
        $voucher_percentage = $this->post('percentage');
        $voucher_status = $this->post('status');
        $voucher_requirement = $this->post('requirement');
        $voucher_description = $this->post('description');
        $voucher_image = $this->post('image');
        $isPersonal = $this->post('isPersonal');
        $stock = $this->post('stock');

        $voucherData = array(
            'voucher_name' => $voucher_name,
            'voucher_startDate' => $voucher_startDate,
            'voucher_endDate' => $voucher_endDate,
            'voucher_status' => $voucher_status,
            'voucher_percentage' => $voucher_percentage,
            'voucher_value' => $voucher_value,
            'voucher_requirement' => $voucher_requirement,
            'voucher_description' => $voucher_description,
            'voucher_image' => $voucher_image,
            'isPersonal_voucher' => $isPersonal,
            'voucher_stock' => $stock,
        );
        $insertStatus = $this->db->insert('voucher', $voucherData);
        if ($insertStatus) {
            $this->sentResponse("Data", "", "Added", 200, $voucherData);
        } else {
            $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401, $voucherData);
        }
    }
}