<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH . 'controllers/ResponseSender.php';

class postUpdateAmenities extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_post() {
        $amen_id = $this->post('id');
        $amen_name = $this->post('name');
        $amen_type = $this->post('type');
        $amen_price = $this->post('price');
        $amen_desc = $this->post('desc');

        $amenitiesData = array(
            'amenities_name' => $amen_name,
            'amenities_type' => $amen_type,
            'amenities_price' => $amen_price,
            'amenities_desc' => $amen_desc,
        );
        $this->db->where('amenities_id', $amen_id);
        $updateStatus = $this->db->update('amenities', $amenitiesData);
        if ($updateStatus) {
            $this->sentResponse("Data", "", "Updated", 200, array("name" => $amen_name, "type" => $amen_type, "price" => $amen_price, "desc" => $amen_desc));
        } else {
            $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401, array("name" => $amen_name, "type" => $amen_type, "price" => $amen_price, "desc" => $amen_desc));
        }
    }
}