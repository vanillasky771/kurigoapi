<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class putDeleteAmenities extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_put() {
        $amen_id = $this->put('id');
        $deleteStatus = $this->db->delete('amenities', array('amenities_id' => $amen_id)); 
        if ($deleteStatus) {
            $this->sentResponse("Data", "", "Deleted", 200, array("amenId" => $amen_id));
        } else {
            $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401, array("amenId" => $amen_id));
        }
    }
}