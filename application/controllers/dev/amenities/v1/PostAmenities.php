<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH . 'controllers/ResponseSender.php';

class postAmenities extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_post() {
        $amen_name = $this->post('name');
        $amen_type = $this->post('type');
        $amen_price = $this->post('price');
        $amen_desc = $this->post('desc');

        $amenitiesData = array(
            'amenities_name' => $amen_name,
            'amenities_type' => $amen_type,
            'amenities_price' => $amen_price,
            'amenities_desc' => $amen_desc,
        );
        $insertStatus = $this->db->insert('amenities', $amenitiesData);
        if ($insertStatus) {
            $this->sentResponse("Data", "", "Added", 200, array("name" => $amen_name, "type" => $amen_type, "price" => $amen_price, "desc" => $amen_desc));
        } else {
            $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401, array("name" => $amen_name, "type" => $amen_type, "price" => $amen_price, "desc" => $amen_desc));
        }
    }
}