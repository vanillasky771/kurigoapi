<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH . 'controllers/ResponseSender.php';

class getVoucher extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_get() {
        $vouchersData = $this->db->get('voucher')->result();
        if (!empty($vouchersData)) {
            $response['Vouchers'] = $vouchersData;
            $this->sentResponse("Vouchers", $vouchersData, "", 200, "");
        } else if (count($vouchersData) == 0){
            $this->sentResponse("Vouchers", [], "", 200,"");
        } else {
            $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401,""); 
        }
    }
}