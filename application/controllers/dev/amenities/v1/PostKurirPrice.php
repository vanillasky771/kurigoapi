<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH . 'controllers/ResponseSender.php';

class postKurirPrice extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_post() {
        $updatedPrice = $this->post('price');
        $serviceType = $this->post('serviceType');
        $token = $this->post('token');
        $credsCheck = $this->db->get_where('credential', array('token' => $token))->result();
        if (count($credsCheck) == 1){
            $array = array(
                "price" => $updatedPrice
            );
            $this->db->set('price', $updatedPrice);            
            $this->db->where('service_type', $serviceType);
            $update = $this->db->update('delivery_pricing');
            if ($update) {
                $this->sentResponse("Data", $array, "Updated", 200, array("type" => $serviceType, "price" => $updatedPrice, "token" => $token));
            } else {
                $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "Data can't be updated", 400,array("type" => $serviceType, "price" => $updatedPrice, "token" => $token));
            }
        }
    }
}