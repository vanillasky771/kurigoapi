<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH . 'controllers/ResponseSender.php';
class getAmenities extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_get() {
        $amenitiesData = $this->db->get('amenities')->result();
        
        if (!empty($amenitiesData)) {
            $this->sentResponse("Amenities", $amenitiesData, "", 200, "");
        } else if (count($amenitiesData) == 0){
            $this->sentResponse("Amenities", [], "", 200, "");
        } else {
            $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401, "");
        }
    }
}