<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class putDeleteVoucher extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_put() {
        $voucher_id = $this->put('id');
        $deleteStatus = $this->db->delete('voucher', array('voucher_id' => $voucher_id)); 
        if ($deleteStatus) {
            $this->sentResponse("Data", "", "Deleted", 200, array("voucherId" => $voucher_id));
        } else {
            $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401, array("voucherId" => $voucher_id));
        }
    }
}