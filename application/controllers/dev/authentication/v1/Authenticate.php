<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class Authenticate extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_post() {
        $username = $this->post('username');
        $passsword = $this->post('password');
        $auth = $this->post('authority');
        
        $checker = $this->db->get_where('credential', array('username' => $username))->result();
        $chekcerValue = count($checker);
        if ($chekcerValue == 1) {
            if (md5($passsword) == array_column($checker,"password")[0]) {
                $token = md5(uniqid($username,true));
                $userId = array_column($checker,"uniqueUser");
                $userAuth = array_column($checker,"authority");
                
                if ($userAuth[0] == "Admin" || $userAuth[0] == "superuser") {
                    $branches = $this->db->get_where('admins', array('username' => $username))->result();
                    $response['Branch'] = $branches[0];
                } else {
                    $response['UserId'] = $userId[0];
                }
                $response['AuthType'] = $userAuth[0];
                $response['Token'] = $token;
                $data = array(
                    'token' => $token,
                    'lastSigned' => date("Y-m-d h:m:s")
                ); 
                $this->db->where('uniqueUser', $userId[0]);
                $this->db->update('credential', $data);
                $this->sentResponse("Data", $response, "", 200, array("username" => $username));
            } else {
                $this->sendErrorResponse("Oops something wrong, try again later!.", 10001, "Oops wrong username / password!.", 404, array("username" => $username));
            }
        } else {
            $this->sendErrorResponse("Oops something wrong, try again later!.", 10005, "Is this really you!.", 404, array("username" => $username));
        }
    }
}