<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class anonymousToken extends ResponseSender {
	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_get() {
        $uid = $this->get('uid');
        $this->sentResponse("Data", array("Token" => md5(uniqid($uid,true))), "", 200,array("uid" => $uid));
    }
}