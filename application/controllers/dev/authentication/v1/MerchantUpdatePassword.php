<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class merchantUpdatePassword extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_post() {
        $oldPass = $this->post('oldPassword');
        $newPass = $this->post('newPassword');
        
        $checker = $this->db->get_where('merchant_credential', array('password' => $oldPass))->result();
        $chekcerValue = count($checker);
        if ($chekcerValue == 1) {
            $data = array(
                'password' => $newPass
            ); 
            $this->db->where('uniqueUser', array_column($checker, "uniqueUser")[0]);
            $this->db->update('merchant_credential', $data);
            $this->sentResponse("Data", "", "Password Updated", 200,"");
        } else {
            $this->sendErrorResponse("Oops something wrong, try again later!.", 10005, "Is this really you!.", 404, "");
        }
    }
}