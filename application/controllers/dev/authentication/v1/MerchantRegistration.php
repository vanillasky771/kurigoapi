<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class merchantRegistration extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_post() {
        $nik = $this->post("nik");
        $namaLengkap = $this->post("namaLengkap");
        $phone = $this->post("phone");
        $email = $this->post("email");
        $merchant_password = $this->post("password");
        $namaBank = $this->post("namaBank");
        $nomorRekening = $this->post("noRek");
        $merchant_name = $this->post("namaMerchant");
        $address = $this->post("address");
        $lat = $this->post("lat");
        $long = $this->post("long");
        $merchant_mainCategory = $this->post("mainCategory");
        $merchant_subCategory = $this->post("subCategory");
        $merchant_jadwalBuka = json_decode($this->post("jadwalBuka"));
        $merchant_logoImage = $this->post("logoResto");
        $merchant_city = $this->post("city");
        $merchant_uniqueCode = $this->post("uniqueCode");
        $this->db->where('merchant_phone' , $phone);
        $checker = $this->db->get('merchant')->result();
        $chekcerValue = count($checker);

        if ($chekcerValue == 0) {
            define('UPLOAD_DIR', "merchantImage/"); //local
            $img = str_replace(' ', '+', $merchant_logoImage);
            $data = base64_decode($img);
            $file = UPLOAD_DIR .$merchant_name.'.png';
            $success=file_put_contents($file, $data);
            if ($success) {
                $updateImage = "merchantImage/".$merchant_name.".png";
            } else {
                $updateImage = $merchant_logoImage;
            }
            $dataRegister = array(
                'merchant_nik' => $nik,
                'merchant_name' => $namaLengkap,
                'merchant_displayName' => $merchant_name,
                'merchant_bankName' => $namaBank,
                'merchant_bankAccNumber' => $nomorRekening,
                'merchant_address' => $address,
                'merchant_lat' => $lat,
                'merchant_long' => $long,
                'merchant_phone' => $phone,
                'merchant_image' => $updateImage,
                'merchant_mainCategory' => $merchant_mainCategory,
                'merchant_subCategory' => $merchant_subCategory,
                'merchant_city' => $merchant_city,
                'merchant_uniqueCode' => $merchant_uniqueCode,
                'merchant_joined' => date("Y-m-d h:m:s")
            );
            $this->db->insert('merchant', $dataRegister);
            $token = md5(uniqid($email,true));
            $data = array(
                'username' => $email,
                'password' => $merchant_password,
                'uniqueUser' => $merchant_uniqueCode,
                'authority' => "MERCHANT",
                'token' => $token,
                'lastSigned' => date("Y-m-d h:m:s")
            ); 
            $this->db->insert('merchant_credential', $data);
            foreach ($merchant_jadwalBuka->schedule as $key) {
                $scheduledata = array(
                    'day' => $key->day,
                    'open' => $key->open,
                    'close' => $key->close,
                    'merchant_id' => $merchant_uniqueCode
                ); 
                $this->db->insert('merchant_schedule', $scheduledata);
            }
            $this->sentResponse("Data", array("MerchantId" => $merchant_uniqueCode, "Token" => $token), "Registered", 200,$dataRegister);
        } else {
            $this->sendErrorResponse("Account Error.", 10002, "Hei, you're already registered", 401, array("email" => $email, "phone" => $phone));
        }
    }
}