<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class customerRegistration extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_post() {
        $CUSTOMER_NAME = $this->post('name');
        $CUSTOMER_EMAIL = $this->post('email');
        $CUSTOMER_USERNAME = $this->post('username');
        $CUSTOMER_PASSWORD = $this->post('password');
        $CUSTOMER_ADDRESS = $this->post('address');
        $CUSTOMER_LAT = $this->post('latitude');
        $CUSTOMER_LONG = $this->post('longitude');
        $CUSTOMER_PHONE = $this->post('phone');
        $CUSTOMER_UNIQUEADRESS = $this->post('uniqueaddress');
        $CUSTOMER_REFERRAL = $this->post('referral');
        $UID = $this->post('uid');
        
        $checker = $this->db->get_where('customer', array('CUSTOMER_EMAIL' => $CUSTOMER_EMAIL))->result();
        $chekcerValue = count($checker);
        if ($chekcerValue == 0) {
            $dataRegister = array(
                'CUSTOMER_NAME' => $CUSTOMER_NAME,
                'CUSTOMER_EMAIL' => $CUSTOMER_EMAIL,
                'CUSTOMER_ADDRESS' => $CUSTOMER_ADDRESS,
                'CUSTOMER_LAT' => $CUSTOMER_LAT,
                'CUSTOMER_LONG' => $CUSTOMER_LONG,
                'CUSTOMER_PHONE' => $CUSTOMER_PHONE,
                'CUSTOMER_UNIQUEADDRESS' => $UID,
                'CUSTOMER_REFERRAL' => $CUSTOMER_REFERRAL,
            );
            $this->db->insert('customer', $dataRegister);
            $token = md5(uniqid($CUSTOMER_USERNAME,true));
            $data = array(
                'username' => $CUSTOMER_USERNAME,
                'password' => md5($CUSTOMER_PASSWORD),
                'uniqueUser' => $UID,
                'authority' => "CUSTOMER",
                'token' => $token,
                'lastSigned' => date("Y-m-d h:m:s")
            ); 
            $this->db->insert('credential', $data);
            $response['CustomerId'] = $UID;
            $response['Token'] = $token;
            $this->sentResponse("Data", $response, "", 200,$dataRegister);
        } else {
            $this->sendErrorResponse("Account Error.", 10002, "Hei, you're already registered", 401, array("email" => $CUSTOMER_EMAIL, "phone" => $CUSTOMER_PHONE));
        }
    }
}