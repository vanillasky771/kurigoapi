<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class getDriverCodeConfirmation extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_get() {
        $refKey = $this->get('refKey');
        $code = $this->get('par');
        
        $checker = $this->db->get_where('driver_credential', array('refKey' => $refKey))->result();
        $chekcerValue = count($checker);
        if ($chekcerValue == 1) {
            if ($code == array_column($checker,"loginCode")[0]) {
                $token = md5(uniqid($refKey,true));
                $response['Token'] = $token;
                $response['driverID'] = array_column($checker, "drivercredential_id")[0];
                $response['UniqID'] = array_column($checker, "uniqueUser")[0];
                $data = array(
                    'token' => $token,
                ); 
                $this->db->where('refKey', $refKey);
                $this->db->update('driver_credential', $data);
                $this->sentResponse("Data", $response, "Success", 200,array("refkey" => $refKey, "code" => $code));
            } else {
                $this->sendErrorResponse("OTP Invalid!.", 10001, "", 407,array("refkey" => $refKey, "code" => $code));
            }
        } else {
            $this->sendErrorResponse("Is this really you!.", 10005, "", 404,array("refkey" => $refKey, "code" => $code));
        }
    }
}