<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class AdminRegister extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_post() {
        $admUsr = $this->post("username");
        $admPsw = $this->post("password");
        $admBrc = $this->post("branch");
        $this->db->where('username' , $admUsr);
        $checker = $this->db->get('admins')->result();
        $chekcerValue = count($checker);
        if ($chekcerValue == 0) {
            $dataRegister = array(
                'username' => $admUsr,
                'password' => md5($admPsw),
                'branch' => $admBrc
            );
            $credsData = array(
                'username' => $admUsr,
                'password' => md5($admPsw),
                'uniqueUser' => uniqid($admUsr),
                'authority' => "Admin",
                'token' => uniqid($admPsw)
            );
            $this->db->insert('admins', $dataRegister);
            $this->db->insert('credential', $credsData);
            $this->sentResponse("Data", [], "Added", 200, array("username" => $admUsr, "branch" => $admBrc));
        } else {
            $this->sendErrorResponse("Account Error.", 10002, "Hei, you're already registered", 401, array("username" => $admUsr, "branch" => $admBrc));
        }
    }
}