<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class driverAuthentication extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }
    
    function index_post() {
        $phone = $this->post('phone');
        $refKey = $this->post('key');
        
        $checker = $this->db->get_where('driver_credential', array('phone' => $phone))->result();
        $chekcerValue = count($checker);
        if ($chekcerValue == 1) {
            $code = rand(1111,9999);
            $data = array(
                'loginCode' => $code,
                'refKey' => $refKey
            ); 
            $this->db->where('phone', $phone);
            $this->db->update('driver_credential', $data);
            $userkey = 'd231e58bd89d';
            $passkey = 'a8ef980da72c8d076e398adb';
            $telepon = $phone;
            $message = "Gunakan kode ".$code." untuk masuk kedalam aplikasi, jangan bagikan kode ini kepada siapapun.";
            $url = 'https://console.zenziva.net/reguler/api/sendsms/';
            $curlHandle = curl_init();
            curl_setopt($curlHandle, CURLOPT_URL, $url);
            curl_setopt($curlHandle, CURLOPT_HEADER, 0);
            curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
            curl_setopt($curlHandle, CURLOPT_POST, 1);
            curl_setopt($curlHandle, CURLOPT_POSTFIELDS, array(
                'userkey' => $userkey,
                'passkey' => $passkey,
                'to' => $telepon,
                'message' => $message
            ));
            $results = json_decode(curl_exec($curlHandle), true);
            curl_close($curlHandle);
            $this->sentResponse("Data", "", "", 200,array("otpCode" => $code));
        } else {
            $this->sendErrorResponse("You are not registered yet!.", 10005, "", 404, array("phone" => $phone, "key" => $refKey));
        }
    }
}