<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class merchantAuthentication extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_post() {
        $email = $this->post('email');
        $hashedPass = $this->post('hashPass');
        
        $checker = $this->db->get_where('merchant_credential', array('username' => $email, 'password' => $hashedPass))->result();
        $chekcerValue = count($checker);
        if ($chekcerValue == 1) {
            $uniq = array_column($checker, "uniqueUser")[0];
            $merchantStatus = $this->db->get_where('merchant', array('merchant_uniqueCode' => $uniq))->result();
            $token = md5(uniqid($email,true));
            $data = array(
                'token' => $token,
                'lastSigned' => date("Y-m-d h:i:s")
            ); 
            $this->db->where('username', $email);
            $this->db->update('merchant_credential', $data);
            foreach($merchantStatus as $result){
                $result->merchant_isApproved = (bool) $result->merchant_isApproved;
            }
            $this->sentResponse("Data", array("Token" => $token, "isApproved" => array_column($merchantStatus, "merchant_isApproved")[0] ), "LoggedIn", 200, $email);
        } else {
            $this->sendErrorResponse("You are not registered yet!.", 10005, "", 404,$email);
        }
    }
}