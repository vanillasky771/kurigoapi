<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class merchantCodeConfirmation extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_get() {
        $token = $this->get('token');
        $code = $this->get('par');
        
        $checker = $this->db->get_where('merchant_credential', array('token' => $token))->result();
        $chekcerValue = count($checker);
        if ($chekcerValue == 1) {
            if ($code == array_column($checker,"codeKey")[0]) {
                $this->sentResponse("Data", array("MerchantID" => array_column($checker, "merchantcredential_id")[0],
                                        "UniqID" => array_column($checker, "uniqueUser")[0]), "Matched", 200,array($code, $token));
            } else {
                $this->sendErrorResponse("OTP Invalid!.", 10001, "", 407,array($code, $token));
            }
            
        } else {
            $this->sendErrorResponse("Is this really you!.Is this really you!.", 10005, "", 404,array($code, $token));
        }
    }
}