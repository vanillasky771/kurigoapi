<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class driverRegistration extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_post() {
        $NAMA_DRIVER = $this->post("name");
        $ALAMAT_DRIVER = $this->post("address");
        $DRIVER_PHONE = $this->post("phone");
        $DRIVER_USERNAME = $this->post("username");
        $DRIVER_PASSWORD = $this->post("password");
        $DRIVER_PROFILEPICTURE = $this->post("imagePath");
        $DRIVER_VEHICLETYPE = $this->post("vehicle_type");
        $DRIVER_PLATNUMBER = $this->post("platNumber");
        $DRIVER_SIMCNUMBER = $this->post("simCNumber");
        $DRIVER_STNKNUMBER = $this->post("stnkNumber");
        // $DRIVER_SIMIMAGEPATH = $this->post("simImagePath");
        // $DRIVER_STNKIMAGEPATH = $this->post("stnkImagePath");
        $DRIVER_UID = $this->post("druid");
        $this->db->where('driver_hp' , $DRIVER_PHONE);
        $checker = $this->db->get('driver')->result();
        $chekcerValue = count($checker);
        if ($chekcerValue == 0) {
            $dataRegister = array(
                'DRIVER_NAME' => $NAMA_DRIVER,
                'DRIVER_ADDRESS' => $ALAMAT_DRIVER,
                'DRIVER_HP' => $DRIVER_PHONE,
                'DRIVER_PROFILEPICTURE' => $DRIVER_PROFILEPICTURE,
                'DRIVER_VEHICLE' => $DRIVER_VEHICLETYPE,
                'DRIVER_PLATENUMBER' => $DRIVER_PLATNUMBER,
                'DRIVER_SIMCNUMBER' => $DRIVER_SIMCNUMBER,
                'DRIVER_STNKNUMBER' => $DRIVER_STNKNUMBER,
                'driver_rank' => "Beginner",
                'driver_rate' => 5.0,
                // 'DRIVER_SIMIMAGEPATH' => $DRIVER_SIMIMAGEPATH,
                // 'DRIVER_STNKIMAGEPATH' => $DRIVER_STNKIMAGEPATH,
                'driver_joined' => date("Y-m-d h:m:s")
            );
            $driverAddStatus = $this->db->insert('driver', $dataRegister);
            if ($driverAddStatus) {
                $token = md5(uniqid($DRIVER_USERNAME,true));
                $data = array(
                    'phone' => $DRIVER_PHONE,
                    'uniqueUser' => $DRIVER_UID,
                    'authority' => "DRIVER",
                    'token' => $token,
                    'lastSigned' => date("Y-m-d h:m:s")
                ); 
                $this->db->insert('driver_credential', $data);
            }
            $this->sentResponse("Data", array("DriverId" => $DRIVER_UID, "Token" => $token), "", 200,$dataRegister);
        } else {
            $this->sendErrorResponse("Account Error.", 10002, "Hei, you're already registered", 401, array("phone" => $DRIVER_PHONE, "address" => $ALAMAT_DRIVER));
        }
    }
}