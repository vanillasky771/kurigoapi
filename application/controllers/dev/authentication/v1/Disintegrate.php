<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class disintegrate extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_post() {
        $token = $this->post('token');
        $checker = $this->db->get_where('credential', array('token' => $token))->result();
        $driverChecker = $this->db->get_where('driver_credential', array('token' => $token))->result();
        $merchantChecker = $this->db->get_where('merchant_credential', array('token' => $token))->result();
        if (count($checker) == 1 ){
            $this->db->set('token', '""', FALSE);
            $this->db->where('token', $token);
            $this->db->update('credential');
            $this->sentResponse("Data", "", "Logout Success", 200,$token);
        } else if (count($driverChecker) == 1) {
            $this->db->set('token', '""', FALSE);
            $this->db->where('token', $token);
            $this->db->update('driver_credential');
            $this->sentResponse("Data", "", "Logout Success", 200,$token);
        } else if (count($merchantChecker) == 1) {
            $this->db->set('token', '""', FALSE);
            $this->db->where('token', $token);
            $this->db->update('merchant_credential');
            $this->sentResponse("Data", "", "Logout Success", 200,$token);
        } else {
            $this->sendErrorResponse("Oops Credential not valid, please login again!.", 10001, "", 402,$token);
        }
    }
}