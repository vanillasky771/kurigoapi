<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class postUpdateInfoPromotion extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_post() {
        $id = $this->post('promotionID');
        $title = $this->post('title');
        $description = $this->post('desc');
        $image = $this->post('image');
        $startDate = $this->post('start');
        $endDate = $this->post('end');
        $promotionType = $this->post('type');
        $token = $this->post('token');
        $dataPromotion = array(
            'promotion_title' => $title,
            'promotion_desc' => $description,
            'promotion_image' => $image,
            'promotion_start' => $startDate,
            'promotion_end' => $endDate,
            'promotion_type' => $promotionType
        );
        $checker = $this->db->get_where('credential', array('token' => $token))->result();
        if (count($checker) == 1 ){  
            $this->db->where('promotion_id', $id);
            $updateStatus = $this->db->update('promotion', $dataPromotion);
            $this->sentResponse("Data", "", "Promotion Updated", 200,$dataPromotion);
        } else {
            $this->sendErrorResponse("Oops Credential not valid, please login again!.", 10001, "", 402, $dataPromotion);
        }
    }
}