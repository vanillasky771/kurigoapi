<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class getInfoPromotion extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_get() {
        $userType = $this->get('type');
        if ($userType == "admin") {
            $promotionData = $this->db->get('promotion')->result();    
        } else {
            $promotionData = $this->db->get_where('promotion',array('promotion_type' => $userType))->result();
        }
        if (!empty($promotionData)) {
            $this->sentResponse("Promotions", $promotionData, "", 200,$userType);
        } else {
            $this->sentResponse("Promotions", [], "", 200,$userType);
        }
    }
}