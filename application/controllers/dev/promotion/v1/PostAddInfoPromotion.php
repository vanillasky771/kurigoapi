<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class postAddInfoPromotion extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_post() {
        $title = $this->post('title');
        $description = $this->post('desc');
        $image = $this->post('image');
        $startDate = $this->post('start');
        $endDate = $this->post('end');
        $promotionType = $this->post('type');
        $token = $this->post('token');
        $dataMessage = array(
            'promotion_title' => $title,
            'promotion_desc' => $description,
            'promotion_image' => $image,
            'promotion_start' => $startDate,
            'promotion_end' => $endDate,
            'promotion_type' => $promotionType
        );
        $checker = $this->db->get_where('credential', array('token' => $token))->result();
        if (count($checker) == 1 ){  
            $this->db->insert('promotion', $dataMessage);
            $this->sentResponse("Data", "", "Promotion Added", 200,$dataMessage);
        } else {
            $this->sendErrorResponse("Oops Credential not valid, please login again!.", 10001, "", 402, $dataMessage);
        }
    }
}