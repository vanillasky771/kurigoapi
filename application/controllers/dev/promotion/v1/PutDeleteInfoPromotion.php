<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
 include APPPATH . 'controllers/ResponseSender.php';

class putDeleteInfoPromotion extends ResponseSender {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_put() {
        $promotionId = $this->put('promotionID');
        $token = $this->put('token');
        $checker = $this->db->get_where('credential', array('token' => $token))->result();
        if (count($checker) == 1 ){  
            $deleteStatus = $this->db->delete('promotion', array('promotion_id' => $promotionId)); 
            if ($deleteStatus) {
                $this->sentResponse("Data", "", "Promotion Deleted", 200,array($token, $promotionId));
            } else {
                $this->sendErrorResponse("Oops something wrong, try again later!.", 10004, "", 401, array($token, $promotionId));
            }
        } else {
            $this->sendErrorResponse("Oops Credential not valid, please login again!.", 10001, "", 402,array($token, $promotionId));
        }
    }
}