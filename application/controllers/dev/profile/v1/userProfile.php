<?php
use chriskacerguis\RestServer\RestController;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/RestController.php';
require APPPATH . 'libraries/Format.php';

class userProfile extends RestController {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_get() {
        $uid = $this->get('uid');
        $this->db->select("*");
        $this->db->where('customer_uniqueAddress', $uid);
        $customerData = $this->db->get('customer')->result();
        if (!empty($customerData)) {
            $responsse['Name'] = $customerData[0]->customer_name;
            $responsse['Email'] = $customerData[0]->customer_email;
            $responsse['CustomerStatus'] = $customerData[0]->customer_status;
            $responsse['Address'] = $customerData[0]->customer_address;
            $responsse['Phone'] = $customerData[0]->customer_phone;
            $responsse['Referral'] = $customerData[0]->customer_referral;
            $responsse['ProfilePicture'] = $customerData[0]->customer_profilepicture;
            $responsse['JoinedSince'] = $customerData[0]->customer_joined;
            $responsse['OrderCount'] = $customerData[0]->customer_orders;
            $this->response($responsse);
        } else {
            $response['Title'] =  "Oops something wrong, try again later!.";
            $response['Code'] = 10004;
            $response['Message'] = "";
            $this->response($response, 401);
        }
    }
}