<?php
use chriskacerguis\RestServer\RestController;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/RestController.php';
require APPPATH . 'libraries/Format.php';

class getVersion extends RestController {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_get() {
        $versionsData = $this->db->get('versions')->result();
        if (!empty($versionsData)) {
            $responsse['Client'] = $versionsData[0]->client_version;
            $responsse['DB'] = $versionsData[0]->db_version;
            $responsse['DEV'] = $versionsData[0]->dev_version;
            $responsse['PROD'] = $versionsData[0]->prod_version;
            $this->response($responsse);
        } else {
            $response['Title'] =  "Oops something wrong, try again later!.";
            $response['Code'] = 10004;
            $response['Message'] = "";
            $this->response($response, 401);
        }
    }
}