<?php
use chriskacerguis\RestServer\RestController;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/RestController.php';
require APPPATH . 'libraries/Format.php';

class updateLocation extends RestController {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_post() {
        $uid = $this->post('uid');
        $lat = $this->post('latitude');
        $long = $this->post('longitude');

        $locationData = array(
            'customer_lat' => $lat,
            'customer_long' => $long,
        );
        $this->db->where('customer_uniqueAddress', $uid);
        $status = $this->db->update('customer', $locationData);
    
        if ($status) {
            $response = "";
            $this->response($response);
        } else {
            $response['Title'] =  "Can't update location!.";
            $response['Code'] = 10007;
            $response['Message'] = "";
            $this->response($response, 401);
        }
    }
}