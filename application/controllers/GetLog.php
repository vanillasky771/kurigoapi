<?php
use chriskacerguis\RestServer\RestController;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/RestController.php';
require APPPATH . 'libraries/Format.php';

class getLog extends RestController {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
        $CI = &get_instance();
        $this->db2 = $CI->load->database('logging', TRUE);
    }

    function index_get() {
        $count = $this->get('count');
        $this->db2->select('*');
        $this->db2->from('apiLog');
        $this->db2->order_by('log_id', 'desc');
        $this->db2->limit($count);
        $logData = $this->db2->get()->result();
        
        
        if (!empty($logData)) {
            $response = $logData;
            $code = 200;
        } 
        $this->response($response, $code);
    }
}