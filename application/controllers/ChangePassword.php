<?php
use chriskacerguis\RestServer\RestController;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/RestController.php';
require APPPATH . 'libraries/Format.php';

class ChangePassword extends RestController {
    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }
//API Address Param : http://localhost/valashokindoapi/ChangePassword?username={value}&oldPass={value}&newPass={value}&token={value}
    function index_get(){
        $id = $this->get('username');
        $oldPass = $this->get('oldPass');
        $newPass = $this->get('newPass');
        $token = $this->get('token');
        $passcrypt = password_hash($newPass, PASSWORD_BCRYPT);
        $data = array(
            'password' => $passcrypt,
        );  
        $this->db->select('password,token');
        $this->db->where('username', $id);
        $cek = $this->db->get('partner')->result();
        $datacek = array_column($cek, "password");
        $tkn = array_column($cek, "token");
        if($token == $tkn[0]){
            if(password_verify($oldPass, $datacek[0])){
                $this->db->where('username',$id);
                $update = $this->db->update('partner', $data);
                if($update){
                    $response['status'] = 200;
                    $response['Note']='Password Updated';
                    $this->response($response, 200);
                }else{
                    $response['status']= 500;
                    $response['Note']='Fail to Change Password';
                    $this->response($response);
                }
            }
        }else{
            $response['status']= 500;
            $response['Note']='Credential Not Valid';
            $this->response($response);
        }
    }
}