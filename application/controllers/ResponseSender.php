<?php
use chriskacerguis\RestServer\RestController;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/RestController.php';
require APPPATH . 'libraries/Format.php';

class ResponseSender extends RestController {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
        $CI = &get_instance();
        $this->db2 = $CI->load->database('logging', TRUE);
    }

    public function sentResponse($dataName = "", $data = nil, $messaage = nil, $code = nil, $param = nil) {
        $response = [$dataName => $data,
                    "Message" => $messaage,
                    "Status" => $code];

        $respArray = array ('status' => $code, 
            'response' => $data);
        $logger = array(
            'url' => current_url(),
            'param' => json_encode($param),
            'response' => json_encode($respArray)
        );
        $this->db2->insert('apiLog', $logger);
        $this->response($response, $code);
    }

    public function sendErrorResponse($title = "", $code = 80000, $message = nil, $status = nil, $param = nil) {
        $response = ["Title" => $title,
                    "Message" => $message,
                    "Code" => $code,
                    "Status" => $status];
        $respArray = array ('status' => $status, 
            'errorMessage' => $title, 'errorCode' => $code);
        $logger = array(
            'url' => current_url(),
            'param' => json_encode($param),
            'response' => json_encode($respArray)
        );
        $this->db2->insert('apiLog', $logger);
        $this->response($response, $status);
    }
}