<?php
use chriskacerguis\RestServer\RestController;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/RestController.php';
require APPPATH . 'libraries/Format.php';

class ApiLogin extends RestController {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }
//API Address: http://localhost/valashokindoapi/login?umail={value}&upass={value}
    function index_get(){
        $email = $this->get('umail');
        $pass = $this->get('upass');
        $this->db->select('password');
        $this->db->select('name');
        if(strstr($email, '@')){
            $this->db->where('email', $email);
        }else{
            $this->db->where('phone', $email);
        }
        
        $cek = $this->db->get('user')->result();
        $datacek = array_column($cek, "password"); 
        $profileName = array_column($cek, "name"); 
        print_r($profileName);
        if(empty($datacek)){
                $response['status'] = 500;
                $response['Note'] = "User not-registered";
                $this->response($response);
        }else{
            if(password_verify($pass, $datacek[0])){
                $response['status'] = 200;
                $response['Note'] = "User Logged-in";
                $response['ProfileName'] = $profileName[0];
                $token = md5(uniqid($email,true));
                $response['Token'] = $token;
                $data = array(
                    'token' => $token,
                ); 
                if(strstr($email, '@')){
                    $this->db->where('email', $email);
                }else{
                    $this->db->where('phone', $email);
                }
                $this->db->update('user', $data);
                $this->response($response);
            }else{
                $response['status'] = 500;
                $response['Note'] = "Wrong Password";
                $this->response($response);
            }
        }
    }
}