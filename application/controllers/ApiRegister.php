<?php
use chriskacerguis\RestServer\RestController;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/RestController.php';
require APPPATH . 'libraries/Format.php';

class ApiRegister extends RestController {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }
//API Address : http://localhost/valashokindoapi/register?email={value}&phone={value}&password={value}&name={value}&refferal={value}&uuid={value}
    function index_get(){
        $pass = $this->get('password');
        $email = $this->get('email');
        $phone = $this->get('phone');

        $this->db->select('email');
        $this->db->where('email', $email);
        $cekPrevuser = $this->db->get('user')->result();
        $this->db->select('phone');
        $this->db->where('phone', $phone);
        $cekPrevuserPhn = $this->db->get('user')->result();
        if(!$cekPrevuser && !$cekPrevuserPhn){
            $passcrypt = password_hash($pass, PASSWORD_BCRYPT);
            $data = array(
                'email' => $email,
                'password' => $passcrypt,
                'phone' => $this->get('phone'),
                'user_uuid' => $this->get('uuid'),
                'name' => $this->get('name'),
                'refferal' => $this->get('refferal'),
                'transaction_count' => 0
            );  
            $insert = $this->db->insert('user', $data);
            if($insert){
                $response['status'] = 200;
                $response['Note']='User Registered';
                $this->response($response);
            }else{
                $response['status']= 500;
                $response['Note']='Fail to Register User';
                $this->response($response);
            }
        }else{
            $response['status']= 201;
                $response['Note']='Sorry, this email / phone number already registered. Please Login to continue';
                $this->response($response);
        }
        
        
    }
}